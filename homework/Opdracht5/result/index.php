<?php


/* Show all errors
------------------------------------------*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

/* Variable
----------------------------*/
$br = "<br>\n";

/* Print to screen
----------------------------*/
echo '1.' . $br;
echo '(-2 + 4) * (25 * 9 + 26) + 25 * X = 652' . $br;
echo 'X = ' . 652 / ((-2 + 4) * (25 * 9 + 26) + 25) . $br . $br;

echo '2.' . $br;
echo '-2 + 4 * 25 * 9 + 26 + 25 * 6 = X' . $br;
echo 'X = ' . (-2 + 4 * 25 * 9 + 26 + 25 * 6) . $br . $br;

echo '3.' . $br;
echo 'Negen maal 758' . $br;
echo '9 x 758 = ' . 9 * 758 . $br . $br;

echo '4.' . $br;
echo '14 gedeeld door 58' . $br;
echo '14 / 58 = ' . 14/58 . $br . $br;

echo '5.' . $br;
echo '345 maal 2 plus 7, waarbij eerst 2 en 7 opgeteld worden' . $br;
echo '345 x (2 + 7) = ' . 345 * (2 + 7) . $br . $br;

echo '6.' . $br;
echo '40 minus het resultaat van 3 maal drie' . $br;
echo '40 - 3 x 3 = ';
echo  40 - 3 * 3 . $br . $br;


echo '7.' . $br;
echo '2 tot de macht 12' . $br;
echo '2<sup>2</sup> = ' . 2 ** 12 . $br . $br;

echo '8.' . $br;
echo 'De som van het resultaat van 2,4 tot de macht 2 en het resultaat van 5 maal 1.9' . $br;
echo '2.4<sup>2</sup> + 5 x 1.9 = ';
echo 2.4 ** 2 + 5 * 1.9 . $br . $br;

echo '9.' . $br;
echo 'De som van 100 en het resultaat van negen en een half gedeeld door drieendertig' . $br;
echo '100 + 9.5 / 33 = ';
echo 100 + 9.5 / 33 . $br . $br;
