@extends( 'layout.master' )

@section('title')
    mijn profile
@endsection

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
            <h1>Hoi Dit is mijn profiel pagina</h1>
            <ul>
              <li>ID: {{ Auth::user()->id }}</li>
              <li>Naam: {{ Auth::user()->naam }}</li>
              <li>Email: {{ Auth::user()->email }}</li>
            </ul>
        </div>
    </div>
@endsection
