@extends( 'layout.master' )

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
            <h1>{{ $news->title }}</h1>
            <span>{{ $news->publish_date }}</span>
            <p>
                {{ $news->body }}
            </p>

            @auth
            <a href="/news/{{ $news->id }}/edit" class="btn btn-primary">Edit</a>
            <!-- Delete bericht -->

            <form action="/news/{{ $news->id  }}" method="post" onsubmit="return confirm('Zeker weten?');" >
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="delete">
              <button class="btn btn-danger" type="submit">Delete</button>
            </form>
            @endauth


            <a href="/news">Nieuws overzicht</a>
        </div>
    </div>
@endsection
