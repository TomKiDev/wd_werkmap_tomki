var gulp = require( 'gulp' ),
    sass = require( 'gulp-sass' ),
    cssmin = require( 'gulp-cssmin' ),
    rename = require( 'gulp-rename' ),
    minify = require('gulp-minify');

gulp.task( 'styles', function () {
  return gulp.src( './resources/assets/sass/**/*.scss' )
      .pipe( sass().on( 'error', sass.logError) )
      .pipe( cssmin() )
      .pipe( rename( { suffix : '.min' } ) )
      .pipe( gulp.dest( './public/css' ) )
} );

gulp.task('compress', function() {
  return gulp.src( './resources/assets/js/**/*.js' )
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    } ) )
    .pipe( rename( { suffix : '.min' } ) )
    .pipe(gulp.dest( './public/js' ) )
} );

gulp.task( 'watch', function () {
	gulp.watch( './resources/assets/sass/**/*.scss', ['styles'] );
  gulp.watch( './resources/assets/js/**/*.js', ['compress'] );
} );

gulp.task('default', ['styles', 'compress']);
