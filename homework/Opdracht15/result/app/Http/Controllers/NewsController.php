<?php

namespace App\Http\Controllers;

use App\News;

class NewsController extends Controller
{
    /**
     * Laat de laatste 5 nieuwsberichten zien
     * Het model zorgt voor de query,
     * hier hoef ik alleen te zeggen hoeveel berichten ik wil
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $messages = News::Latest(5);

        return view('news.index',
            compact('messages')
        );
    }

    /**
     * @param \App\News $news
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show( $slug )
    {

        $news = News::Active( $slug );

        return view('news.show',
            compact('news')
        );
    }

    /**
     * @param \App\News $news
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
      $news = new News;

      if( session()->hasOldInput() ) {
        $news->title        = session()->getOldInput( 'title' );
        $news->publish_date = session()->getOldInput( 'publish_date' );
        $news->body         = session()->getOldInput( 'body' );
      }

      return view('news.create', compact('news') );
    }

    /**
     * @param \App\News $news
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store()
    {
      // form validatie:

      $this->validate( request(),[
        'title'         =>  'required|min:2|max:150',
        'publish_date'  =>  'required|date',
        'body'          =>  'required'
      ]);

      // dd( request()->all() ); // Alles wat gestuurd is terug zien
      /* Een lange manier:
      $news                 = new News;
      $news->title          = request( 'title' );
      $news->publish_date   = request( 'publish_date' );
      $news->body           = request( 'body' );

      $news->save();
      */

      // News::create( request()->all() );
      News::create( request(['title', 'publish_date', 'body']) );

      return redirect( '/news' );

    }

    public function delete( $news_id )
    {

      $news   =  News::find( $news_id );
      $title  = $news->title;
      $news->delete();

      return redirect( '/news' )->with( 'message', 'News bericht' . $title . ' is verwijderd' );
    }

    // Edit berciht
    public function edit( News $news )
    {

      if( session()->hasOldInput() ) {
        $news->title        = session()->getOldInput( 'title' );
        $news->slug         = session()->getOldInput( 'slug' );
        $news->publish_date = session()->getOldInput( 'publish_date' );
        $news->body         = session()->getOldInput( 'body' );
      }
      
      return view('news.edit',
          compact('news')
      );
    }

    public function update( News $news )
    {
      // form validatie:

      $this->validate( request(),[
        'title'         =>  'required|min:2|max:150',
        'slug'          =>  'required',
        'publish_date'  =>  'required|date',
        'body'          =>  'required'
      ]);

      $news->update( request( [ 'title', 'slug', 'publish_date', 'body']) );

      return redirect( '/news/' . $news->slug )-> with( 'message', 'je bericht is aangepast' );

    }
}
