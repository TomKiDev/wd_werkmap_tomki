<?php
/* Show all errors
------------------------------------------*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

/* Variables
------------------------------------------*/
$post_text = 'Default text';
$autos = ['Tesla', 'Lexus', 'BMW', 'Audi'];
$title = 'Pagina title';
$post_auto = 'Tesla';

/* Set posted information
------------------------------------------*/
if( isset( $_POST['text'] ) ) {
	$post_text 		= $_POST['text']; // Set input text
	if ( $post_text == '' ) { // If it's empty
		$post_text = 'Default text'; // Set default text back
	}
	$post_auto 	= $_POST['autos']; // Set input auto
	$title 				= $post_text; // Set input text
}

/* Show the posted information
------------------------------------------*/
if( isset( $_POST['text'] ) ) {
	echo '<pre>';
	print_r( $_POST );
	echo '</pre>';
}
?>

<!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Page title
	===================================-->
	<title><?php echo $title; ?></title>
	<!-- Styles
	===================================-->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
</head>
<body>
	<!-- Form
	===================================-->
	<form method="post">
		Tekst<br>
		<textarea type="text" name="text" placeholder="<?php echo $post_text; ?>"></textarea><br>
		Autos:<br>
		<select name="autos">
		<?php
			/* Create select from array
			------------------------------------------*/
			foreach ($autos as $auto) {
				echo '<option';
				if ( $auto == $post_auto) {
					echo ' selected="selected"';
				}
				echo '>' . $auto . '</option>';
			}
		?>
	  </select><br>
		<br>
		<input type="submit" value="Submit" />
	</form>
</body>
