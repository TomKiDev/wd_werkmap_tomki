<?php
/* Start session if not existing
------------------------------------------*/
if ( ! session_id() ) {
    session_start();
}

/* Show all errors
------------------------------------------*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

/* Define root and constants
------------------------------------------*/

// If at develop stage at home
if (strpos($_SERVER['DOCUMENT_ROOT'], '/home/vagrant/code/cmmworking/public') !== FALSE)
{
  define( 'DOCUMENT_ROOT' , '/home/vagrant/code/cmmworking/public/Opdracht14' );
  define( 'DB_HOST' , '127.0.0.1');
	define( 'DB_USER' , 'homestead');
	define( 'DB_CRED' , 'secret');
	define( 'DB_NAME' , 'wdproject');
}
// If ready to check for Git
elseif (strpos($_SERVER['DOCUMENT_ROOT'], 'F:/WDV-17/WD_werkmap_tomki/homework') !== FALSE)
{
  define( 'DOCUMENT_ROOT' , 'F:/WDV-17/WD_werkmap_tomki/homework/Opdracht14' );
  define( 'DB_HOST' , '127.0.0.1');
	define( 'DB_USER' , 'homestead');
	define( 'DB_CRED' , 'secret');
	define( 'DB_NAME' , 'wdproject');
}
// If at school
elseif (strpos($_SERVER['DOCUMENT_ROOT'], '/Volumes/CMMTomerK/WDV-17/working') !== FALSE)
{
  define( 'DOCUMENT_ROOT' , '/Volumes/CMMTomerK/WDV-17/working/homework13Jan/Opdracht14' );
  define( 'DB_HOST' , '127.0.0.1');
	define( 'DB_USER' , 'homestead');
	define( 'DB_CRED' , 'secret');
	define( 'DB_NAME' , 'wdproject');
}
// fallback
else
{
  define( 'DOCUMENT_ROOT' , $_SERVER['DOCUMENT_ROOT'] . '/opdracht14' );
  define( 'DB_HOST' , '127.0.0.1');
	define( 'DB_USER' , 'homestead');
	define( 'DB_CRED' , 'secret');
	define( 'DB_NAME' , 'wdproject');
}

define( 'WEB_ROOT' , 'http://dev.cmmworking.nl' ); // Web root
define( 'SITE_ROOT' , DOCUMENT_ROOT . '/public' ); // Site root
define( 'LIBRARY' , DOCUMENT_ROOT . '/Library' ); // Library
define( 'MAIN' , LIBRARY . '/views/main' ); // Main
define( 'PAGES' , WEB_ROOT . '/Opdracht14/Library/Views/Pages' ); // Pages
define( 'NEWS' , LIBRARY . '/Opdracht14/Library/Views/News' ); // News
define( 'WEB_NEWS' , WEB_ROOT . '/Opdracht14/Library/Views/News' ); // News
define( 'PUBLIC_ROOT' , WEB_ROOT . '/Opdracht14/public' ); // Public
define( 'IMG' , PUBLIC_ROOT . '/images' ); // Images
define( 'IMG_ROOT' , SITE_ROOT . '/images' ); // Images
define( 'CSS' , PUBLIC_ROOT . '/css' ); // CSS
define( 'JS' , PUBLIC_ROOT . '/js' ); // JS
define( 'BR' , "<br>\n" ); // line break
