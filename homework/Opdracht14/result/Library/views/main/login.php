<?php
  /* Connect to Database
  ---------------------------*/
  require_once( DOCUMENT_ROOT . '/Library/Services/Database.php' );
  $mysqlConnection = ConnectToDb();
  /* Get User from Database
  ---------------------------*/
  require_once( LIBRARY . '/Controllers/UserController.php' );

  if( isset( $_POST['email'] ) && isset( $_POST['password'] ) ) {
    $userCheck = new UserController($mysqlConnection);
    $sessionUser = $userCheck->checkUserLogin( $_POST['email'], $_POST['password'] );
    // echo '<pre>';
    // print_r($sessionUser);
    // echo '</pre>';
    if ( ! empty( $sessionUser) ) {
      $_SESSION['ingelogd'] = 'Ja';
      $_SESSION['user_id'] = $sessionUser->getId();
      $_SESSION['user'] = $sessionUser->getUserName();


    } else {
      $_SESSION['ingelogd'] = 'nee';
      echo "<script type='text/javascript'>alert('Email or password are incorrect, please check your input and try again');</script>";
    }
  }
  if ( isset( $_POST['signOut'] ) && $_POST['signOut'] == 'signOut' ) {
    $_SESSION['ingelogd'] = 'Nee';
    unset($_SESSION['ingelogd']);
    unset($_SESSION['user_id']);
    unset($_SESSION['user']);
    unset($_SESSION['img']);
    $_SESSION['img']    = IMG . '/cmm-logo@.png';
  }


  if ( isset( $_SESSION['ingelogd'] ) && ('Ja' == $_SESSION['ingelogd']) ) {
    // echo '<pre>';
    // print_r($_SESSION);
    // echo '</pre>';

    echo '<form class="navbar-form navbar-right" method="post">';
    echo '<button type="submit" name="signOut" value="signOut" class="btn btn-success">User '. $_SESSION['user'] . ' Sign out</button>';
    echo '</form>';
  } else {
    echo '<form class="navbar-form navbar-right" method="post">';
    echo '<div class="form-group">';
    echo '<input type="text" name="email" placeholder="Email" class="form-control">';
    echo '</div>';
    echo '<div class="form-group">';
    echo '<input type="password" name="password" placeholder="Password" class="form-control">';
    echo '</div>';
    echo '<button type="submit" name="signIn" class="btn btn-success">Sign in</button>';
    echo '</form>';
  }
