<?php
/* Page functions
---------------------------*/
require_once( LIBRARY . '/Functions/functions.php' );
?>
<!DOCTYPE html>
<html lang="nl">
<!-- Head
===============================-->
<head>

  <meta charset="utf-8">
  <link rel="shortcut icon" href="<?= IMG ?>/favicon.ico">

  <!-- Page title -->
  <?php
  if ( ! isset($page)) {
    $page = 'welkom';
  }
  ?>

  <title><?php echo createTitle($page); ?></title>
  <!-- Styles -->
  <link href="<?=CSS?>/bootstrap.css" rel="stylesheet">
  <link href="<?=CSS?>/main.css" rel="stylesheet">
</head>
<!-- Body
===============================-->
<body>
<!-- Header
===============================-->
  <header>
    <!-- Navigatie
    ===============================-->
    <?php
      require_once( MAIN . '/nav.php' );
    ?>
