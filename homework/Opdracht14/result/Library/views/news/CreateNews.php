

    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <h1>Maak een nieuw bericht aan: </h1>
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <?php
      if ( empty($_POST['title']) ) {
        $action = WEB_NEWS . '/news.php?session_id=' . $_GET['session_id'] . '&create=1' ;
      } else {
        $url = str_replace(array(' ', '.'), array('_', ''), $_POST['title']);
        $url = strToLower( urlencode( $url) );
        $action = WEB_NEWS . '/news.php?news=' . $url . '&session_id=' . $_GET['session_id'] . '&editor=1';
      }

      // echo '<pre>';
      // print_r($_POST);
      // print_r($_GET);
      // echo '</pre>';
        if( ( !empty($_GET['session_id']) ) && ( ( '1' == $_GET['session_id'] )  || ( '2' == $_GET['session_id']) ) )
        {
          echo '<form action="' . $action . '" method="post">';
          echo '<p>Titel</p>';
          echo "<input type='text' name='title' size='100' placeholder='Titel' value=''>";
          echo '<p>Category</p>';
          echo '<select name="category_id">';
          echo '<option value="1">AJAX</option>';
          echo '<option value="2">PSV</option>';
          echo '<option value="3">AZ</option>';
          echo '<option value="5">Sport</option>';
          echo '<option value="6">Weer</option>';
          echo '<option value="7">Regen</option>';
          echo '<option value="8">Zon</option>';
          echo '<option value="9">Techniek</option>';
          echo ' <option value="10">Media</option>';
          echo ' <option value="11">Algemeen</option>';
          echo ' <option value="12">Binnenland</option>';
          echo ' <option value="13">Buitenland</option>';
          echo ' <option value="14">Regio</option>';
          echo ' <option value="15">Politiek</option>';
          echo ' <option value="16">Economie</option>';
          echo ' <option value="17">Tech</option>';
          echo '</select>';
          echo '<p>PublishDate</p>';
          echo '<input type="text" name="publishDate" placeholder="' . date('Y-m-d H:i:s') . '" value="' . date('Y-m-d H:i:s') . '">';
          echo '<p>Thumbnail</p>';
          echo '<input type="text" name="imageSrc" placeholder="Image source" value="voorbeeld.jpg">';
          echo '<p>Intro</p>';
          echo '<textarea rows="4" cols="100" name="intro" placeholder="Intro here"></textarea>';
          echo '<p>Content</p>';
          echo '<textarea rows="20" cols="100" name="content" placeholder="Intro here"></textarea>';
          echo '<hr>';
          echo '<p><button type="submit" name="editNews" value="editNews" class="btn btn-success">Bewerk bericht</button></p>';
          echo '</form>';
        }


        if ( isset( $_POST['editNews'] ) ) {
          $result = $newsController->CreateSingleNews( $_GET['session_id'], $_POST ) ;
          echo '<h2>' . $result . '</h2>';
        }
      ?>


    </div>
