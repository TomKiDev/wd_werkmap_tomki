

    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <p>Bewerk bericht met de title: </p>
        <h1><?php echo $newsPage->getTitle(); ?></h1>
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <?php

      if ( empty($_POST['title']) ) {
        $action = WEB_NEWS . '/news.php?news=' . $_GET['news'] . '&session_id=' . $_GET['session_id'] . '&editor=' . $_GET['editor'] ;
      } else {
        $url = str_replace(array(' ', '.'), array('_', ''), $_POST['title']);
        $url = strToLower( urlencode( $url) );
        $action = WEB_NEWS . '/news.php?news=' . $url . '&session_id=' . $_GET['session_id'] . '&editor=1';
      }
      // $result['action'] = '';

      // echo '<pre>';
      // print_r($_POST);
      // print_r($_GET);
      // echo '</pre>';
        if( ( !empty($_GET['session_id']) ) && ( ( '1' == $_GET['session_id'] )  || ( '2' == $_GET['session_id']) ) )
        {
          echo '<form action="' . $action . '" method="post">';
          echo "<input type='hidden' name='newsId' value='" . $newsPage->getID() . "'>";
          echo '<p>Titel</p>';
          echo "<input type='text' name='title' size='100' placeholder='Titel' value='" . $newsPage->getTitle() . "'>";
          echo '<p>Category</p>';
          echo '<select name="category_id">';
          echo '<option value="1"';
          if($newsPage->getCategoryId()== 1){ echo ' selected';}
          echo '>AJAX</option>';
          echo '<option value="2"';
          if($newsPage->getCategoryId()== 2){ echo ' selected';}
          echo '>PSV</option>';
          echo '<option value="3"';
          if($newsPage->getCategoryId()== 3){' selected';}
          echo '>AZ</option>';
          echo '<option value="5"';
          if($newsPage->getCategoryId()== 5){' selected';}
          echo '>Sport</option>';
          echo '<option value="6"';
          if($newsPage->getCategoryId()== 6){' selected';}
          echo '>Weer</option>';
          echo '<option value="7"';
          if($newsPage->getCategoryId()== 7){' selected';}
          echo '>Regen</option>';
          echo '<option value="8"';
          if($newsPage->getCategoryId()== 8){' selected';}
          echo '>Zon</option>';
          echo '<option value="9"';
          if($newsPage->getCategoryId()== 9){' selected';}
          echo '>Techniek</option>';
          echo ' <option value="10"';
          if($newsPage->getCategoryId()== 10){' selected';}
          echo '>Media</option>';
          echo ' <option value="11"';
           if($newsPage->getCategoryId()== 11){' selected';}
           echo '>Algemeen</option>';
          echo ' <option value="12"';
          if($newsPage->getCategoryId()== 12){' selected';}
          echo '>Binnenland</option>';
          echo ' <option value="13"';
          if($newsPage->getCategoryId()== 13){' selected';}
          echo '>Buitenland</option>';
          echo ' <option value="14"';
          if($newsPage->getCategoryId()== 14){' selected';}
          echo '>Regio</option>';
          echo ' <option value="15"';
          if($newsPage->getCategoryId()== 15){' selected';}
          echo '>Politiek</option>';
          echo ' <option value="16"';
          if($newsPage->getCategoryId()== 16){' selected';}
          echo '>Economie</option>';
          echo ' <option value="17"';
          if($newsPage->getCategoryId()== 17){' selected';}
          echo '>Tech</option>';
          echo '</select>';
          echo '<p>PublishDate</p>';
          echo '<input type="text" name="publishDate" placeholder="' . date('Y-m-d H:i:s') . '" value="' . $newsPage->getPublishDate() . '">';
          echo '<p>Thumbnail</p>';
          echo '<input type="text" name="imageSrc" placeholder="Image source" value="' . $newsPage->getImageSource() . '">';
          echo '<p>Intro</p>';
          echo '<textarea rows="4" cols="100" name="intro" placeholder="Intro here">';
          echo $newsPage->getIntro();
          echo '</textarea>';
          echo '<p>Content</p>';
          echo '<textarea rows="20" cols="100" name="content" placeholder="Intro here">';
          echo $newsPage->getMessage();
          echo '</textarea>';
          echo '<hr>';
          echo '<p><button type="submit" name="editNews" value="editNews" class="btn btn-success">Bewerk bericht</button></p>';
          echo '</form>';
        }


        if ( isset( $_POST['editNews'] ) ) {
          $result = $newsController->EditSingleNews( $_GET['session_id'], $_POST ) ;
          echo '<h2>' . $result . '</h2>';
          // echo '<script>function refresh() { location.replace(' . $action . ') };</script>'; onclick="refresh()"
          // $_SERVER['REQUEST_URI'] = $action;
          // header("Location:" . $action );
        }
      ?>


    </div>
