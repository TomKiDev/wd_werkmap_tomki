<?php
    $newsList = $newsController->getAllNews();
      if( ! empty($newsList) ) {
        foreach( $newsList as $newsModel ) {
          if ( ! empty( $newsModel->getTitle() )) {
            echo '<div class="col-lg-4">';
            echo '<h2>' . $newsModel->getTitle() . '</h2>';
            echo '<p>' . $newsModel->getPublishDate() . '</p>';
            echo '<p>' . $newsModel->getIntro() . '</p>';
            echo '<p><a class="btn btn-default" href="' . WEB_NEWS . '/news.php?news=' . $newsModel->getPostUrl() . '">View details &raquo;</a></p>';
            echo '</div>';
          }
        }
      } else {
        echo 'Helaas niets kunnen vinden';
      }
?>
