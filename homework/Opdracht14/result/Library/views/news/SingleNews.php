    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <h1><?php echo $newsPage->getTitle(); ?></h1>
        <p><small><?php echo $newsPage->getCategoryTitle(); ?></small><p>
        <p><?php echo $newsPage->getUserName(); ?><p>
        <p>Bericht Datum: <?php echo $newsPage->getPublishDate(); ?><p>
        <p>Aangepast Om: <?php echo $newsPage->getLastUpdated(); ?><p>
        <?php
          if (! empty( $_SESSION['user'] ) ) {
            echo '<a href="' . WEB_NEWS . '/news.php?news=' . $newsPage->getPostUrl() . '&session_id=' . $_SESSION['user_id'] . '&editor=1" class="btn btn-primary btn-lg">Bewerk bericht</a>';
            echo '<a href="' . WEB_NEWS . '/news.php?session_id=' . $_SESSION['user_id'] . '&create=1" class="btn second-btn btn-primary btn-lg">Maak een nieuw bericht bericht</a>';
          }
        ?>
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <p><b><?php echo $newsPage->getIntro(); ?></b><p>
      <p><?php echo $newsPage->getMessage(); ?><p>
    </div>
