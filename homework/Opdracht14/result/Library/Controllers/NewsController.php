<?php
/* Require News Model
---------------------------*/
require_once( DOCUMENT_ROOT . '/Library/Models/News.php');

/* News Controller functions
---------------------------*/
class NewsController
{
  /* variables
  ------------------------------------------*/
  protected $_mysqli;

  private function connectToDb()
  {
    if( empty($this->_mysqli) ) {
      // Make connection
      $this->_mysqli = new mysqli( DB_HOST, DB_USER, DB_CRED, DB_NAME );

      // Check connection
      if($this->_mysqli->connect_errno)
      {
        die("
          <h1>Er is iets fout gegaan</h1>
          <p>Verbinding met de database was mislukt</p>
          <p>Error " . $this->_mysqli->connect_errno . " : " . $this->_mysqli->connect_error . "</p>
          <p>Ga terug en probeer later nogmaals</p>
          ");
      }
    }
    // Return the connection object
    return $this->_mysqli;
  }

  /**
  * Get all the news messages
  *
  * @return array of messages
  */
  public function getAllNews()
  {
    // Make connection
    $mysqli = $this->connectToDb();
    // query for all news messages
    $query = " SELECT p.*, c.title AS category, concat( u.username, ' ', u.middlename, ' ', u.lastname ) AS user
            FROM `posts` AS p
            INNER JOIN `categories` AS c ON c.id=p.category_id
            LEFT JOIN `users` AS u ON u.id=p.user_id
            WHERE p.`publish_date` IS NOT NULL
            AND p.`publish_date` <= NOW()
            ORDER BY `publish_date`";

    // Make messages array
    $allNews = array();
    // if any result found
    if( $result = $mysqli->query($query) ) {
      // loop threw the result to make the array of messages
      while ( $news = $result->fetch_assoc() )
      {
        // Make News message model
        $newsObject = new NewsModel( $news );
        // Add to the messages array
        $allNews[] = $newsObject;
      }
      // Return all the messages
      return $allNews;
    }
  }





  /**
  * Show all the news messages
  *
  * @return list of HTML
  */
  public function showNewsList()
  {
    $newsList = $this->getAllNews();
    require_once( NEWS . 'NewsListPage.php' );
  }



  /**
  * Show one news message according to id or url
  *
  * @return newsModel of HTML
  */
  public function showNewsMessage( $newsUrl )
  {
    // Make connection
    $mysqli = $this->connectToDb();
    // echo '<pre>';
    // echo '<p>connectToDb():</p>';
    // print_r(connectToDb());
    // echo '<p>$this:</p>';
    // print_r($this);
    // echo '<p>$mysqli:</p>';
    // print_r($mysqli);
    // echo '</pre>';
    // query for news message based on url
    if ( ! $query = $mysqli->prepare("SELECT p.*, c.title AS category, concat( u.username, ' ', u.middlename, ' ', u.lastname ) AS user
            FROM `posts` AS p
            INNER JOIN `categories` AS c ON c.id=p.category_id
            LEFT JOIN `users` AS u ON u.id=p.user_id
            WHERE p.`publish_date` IS NOT NULL
            AND p.`publish_date` <= NOW()
            AND (p.post_url=? OR p.id=?)")
			) {
			throw new Exception("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // bind the get url
    $query->bind_param("si", $newsUrl['news'], $newsUrl['id'] );
    // echo '<pre>';
    // print_r($newsUrl);
    // echo '</pre>';


    // excute query
    if ( ! $query->execute()) {
			throw new Exception("Execute failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // Get the result
    $result = $query->get_result();
    $item = $result->fetch_assoc();

    // Assign result to news model
    $newsPage = new newsModel( $item );

    // Check no news message found set title to nothing found
    if ( empty( $newsPage->getTitle() ) ) {
      $newsPage->setTitle('Helaas geen bericht kunnen vinden');
    }
    // Return news model
    return $newsPage;
  }






  /**
  * Check user
  *
  * variable session id
  *
  * @return boolean
  */
  public function checkUser( $sessionId )
  {
    // Make connection
    $mysqli = $this->connectToDb();
    // query to match user id
    // echo '<pre>';
    // echo '<p>connectToDb():</p>';
    // print_r(connectToDb());
    // echo '<p>$this:</p>';
    // print_r($this);
    // echo '<p>$mysqli:</p>';
    // print_r($mysqli);
    // echo '</pre>';

    if ( ! $query = $mysqli->prepare("SELECT id FROM users WHERE id = ?") ) {
			throw new Exception("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // bind the get url
    $query->bind_param("i", $sessionId );


    // excute query
    if ( ! $query->execute()) {
			throw new Exception("Execute failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // Get the result
    $result = $query->fetch();
    // $result = $query->get_result();

    // Check the user is editor
    if ( $result == 1 || $result == 2 ) {
      return TRUE;
    }
  }





  /**
  * Edit nieuws bericht
  *
  * param variable session id
  * param array news post
  *
  * @return update newsModel in database
  */
  public function EditSingleNews( $sessionId, $news )
  {
    // Check user premission
    if( $control = $this->checkUser( $sessionId ) == TRUE ){
      // Make connection
      $mysqli = $this->connectToDb();

      $url = str_replace(array(' ', '.'), array('_', ''), $news['title']);
      $url = strToLower( urlencode( $url) );
      $now = date('Y-m-d H:m:s');

      // query to edit news
      if ( ! $query = $mysqli->prepare("UPDATE posts
                SET id= ?, category_id= ?, user_id=?, post_url=?, title=?, intro=?, message=?, image_source=?, publish_date=?, last_updated=?
                WHERE id = ?")
        ) {
        throw new Exception("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
      }

      // bind the get url
      $query->bind_param("iiisssssssi", $news['newsId'], $news['category_id'], $sessionId, $url, $news['title'], $news['intro'], $news['content'], $news['imageSrc'], $news['publishDate'], $now, $news['newsId'] );


      // excute query
      if ( ! $query->execute()) {
        throw new Exception("Execute failed: (" . $mysqli->errno . ") " . $mysqli->error);
      }
      $result = "Bericht is aangepast";
      return $result;
    }
  }




  /**
  * Create nieuws bericht
  *
  * param variable session id
  * param array news post
  *
  * @return new newsModel in database
  */
  public function CreateSingleNews( $sessionId, $news )
  {
    // Check user premission
    if( $control = $this->checkUser( $sessionId ) == TRUE ){
      // Make connection
      $mysqli = $this->connectToDb();

      $url = str_replace(array(' ', '.'), array('_', ''), $news['title']);
      $url = strToLower( urlencode( $url) );
      $now = date('Y-m-d H:m:s');

      // query to insert news to DB
      if ( ! $query = $mysqli->prepare("INSERT INTO posts (category_id, user_id, post_url, title, intro, message, image_source, publish_date, creation_date)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);")
        ) {
        throw new Exception("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
      }

      // bind the get url
      $query->bind_param("iisssssss", $news['category_id'], $sessionId, $url, $news['title'], $news['intro'], $news['content'], $news['imageSrc'], $news['publishDate'], $now );


      // excute query
      if ( ! $query->execute()) {
        throw new Exception("Execute failed: (" . $mysqli->errno . ") " . $mysqli->error);
      }
      $result = "Bericht is aangemakt";
      return $result;
    }
  }



}
