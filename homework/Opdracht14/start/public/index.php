<?php
// die($_SERVER['DOCUMENT_ROOT']);
/* Page settings
---------------------------*/
require_once( '../Library/Settings/config.php' );
/* Connect to Database
---------------------------*/
require_once( DOCUMENT_ROOT . '/Library/Services/Database.php' );
$mysqlConnection = ConnectToDb();
/* Get news from Database
---------------------------*/
require_once( DOCUMENT_ROOT . '/Library/Controllers/NewsController.php' );
$newsController = new NewsController( $mysqlConnection );

/* Set page and call header
---------------------------*/
$page = 'home';
require_once( MAIN . '/head.php' );
?>
    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <?php
          $h1 = 'My first php project';
          $h1 = strtolower($h1);
          $h1 = ucfirst($h1);
        ?>

        <h1><?php echo $h1; ?></h1>
        <p>Welcome to my home page</p>
        <p><a href="<?=PAGES?>/aboutme.php" class="btn btn-primary btn-lg">Read more about me&raquo;</a></p>
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <?php
      include_once( LIBRARY . '/views/news/NewsList.php' );
      ?>

    </div>
    <hr>
    <!-- Footer
    ===============================-->
    <?php
      require_once( MAIN . '/footer.php' );
    ?>
