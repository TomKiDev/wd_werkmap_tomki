<?php
/* Page settings
---------------------------*/
require_once( '../../Settings/config.php' );
/* Connect to Database
---------------------------*/
require_once( DOCUMENT_ROOT . '/Library/Services/Database.php' );
$mysqlConnection = ConnectToDb();
/* Get news from Database
---------------------------*/
require_once( DOCUMENT_ROOT . '/Library/Controllers/NewsController.php' );
$newsController = new NewsController( $mysqlConnection );


/* Set page and call header
---------------------------*/
$page = 'news';
require_once( MAIN . '/head.php' );



if ( ! empty($_GET['news']) || ! empty($_GET['id']) ) {
  $newsPage = $newsController->showNewsMessage($_GET);
  require_once('SingleNews.php');
} else {
  include_once('NewsListPage.php');
}


?>

    </div>
    <hr>
    <!-- Footer
    ===============================-->
    <?php
      require_once( MAIN . '/footer.php' );
    ?>
