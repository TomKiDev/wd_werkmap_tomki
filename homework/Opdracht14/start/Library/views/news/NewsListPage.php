
    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <?php
          $h1 = 'CMM Nieuws';
          $h1 = strtolower($h1);
          $h1 = ucfirst($h1);

        ?>
        <h1><?php echo $h1 ?></h1>
        <p>Lees meer over de laatste ontwikkelingen bij CMM</p>
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <?php
      require_once('NewsList.php');
      ?>
    </div>
