<?php
/* Require User Model
---------------------------*/
require_once( DOCUMENT_ROOT ."/Library/Models/User.php");

/* User Controller functions
---------------------------*/
class UserController
{
  /* variables
  ------------------------------------------*/
  protected $_mysqli;

  private function ConnectToDb()
  {
    if( empty($this->_mysqli) ) {
      // Make connection
      $this->_mysqli = new mysqli( DB_HOST, DB_USER, DB_CRED, DB_NAME );

      // Check connection
      if($this->_mysqli->connect_errno)
      {
        die("
          <h1>Er is iets fout gegaan</h1>
          <p>Verbinding met de database was mislukt</p>
          <p>Error " . $this->_mysqli->connect_errno . " : " . $this->_mysqli->connect_error . "</p>
          <p>Ga terug en probeer later nogmaals</p>
          ");
      }
      // Return the connection object
      return $this->_mysqli;
    }
  }

  /**
  * Check user
  *
  * @return string of messages
  */
  public function checkUserLogin($email, $password) {
    // Make connection
    $mysqli = $this->connectToDb();
    // query for user based on email
    if ( ! $query = $mysqli->prepare("SELECT *
            FROM `users`
            WHERE `email` = ?
            AND `password` = ?")
			) {
			throw new Exception("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}
    sanitize($email);
    sanitize($password);

    $salt1  = "t%^l";
    $salt2  = "6$2!";
    $readyPass = $salt1 . $password . $salt2;
    $checkPass = hash('ripemd128', $readyPass);

    // bind the get url
    $query->bind_param("ss", $email, $checkPass );


    // excute query
    if ( ! $query->execute()) {
			throw new Exception("Execute failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // Get the result
    $result = $query->get_result();
    $item = $result->fetch_assoc();
    // echo '<pre>';
    // print_r($item);
    // echo '</pre>';

    // Return news model if found
    if ( ! empty( $item ) ) {
      // Assign result to news model
      $user = new UserModel( $item);
      return $user;
    }
  }
}
