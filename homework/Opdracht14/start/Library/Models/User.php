<?php

/* User model class
------------------------------------------*/

class UserModel
{
  /* variables
  ------------------------------------------*/
  protected $_id;
  protected $_user_name;
  protected $_password;
  protected $_first_name;
  protected $_middle_name;
  protected $_last_name;
  protected $_gender;
  protected $_email;
  protected $_registration_date;

  /* Construct
  ------------------------------------------*/
  public function __construct ( $defaultVars=array() )
	{
		// Loop threw the array
		foreach( $defaultVars as $key => $value )
		{
			// turn key name to name
			$key = str_replace(" ", "", ucwords( str_replace("_", " ", $key) ) );
			$setter = "set". ucfirst($key);

			// check if setter exists inside the class
			if ( method_exists($this, $setter) ) {
				$this->$setter( $value );
			}
		}
	}

    /* Get and Set
    ------------------------------------------*/

    /* id
    ------------------------------------------*/

    /**
    * Set the _id
    *
    * @param Int $id
    */

    public function setId($id)
    {
      if ( is_numeric($id) ) {
        $this->_id = $id;
      }
    }

    /**
    * Get the id
    *
    * @return Int
    */

    public function getId()
    {
        if ( ! empty($this->_id) )
        {
          return $this->_id;
        } else {
          return NULL;
        }
    }

    /* user name
    ------------------------------------------*/

    /**
    * Set the _user_name
    *
    * @param string $userName
    */

    public function setUserName($userName)
    {
        $this->_user_name = $userName;
    }

    /**
    * Get the _user_name
    *
    * @return string
    */

    public function getUserName()
    {
        if ( ! empty($this->_user_name) )
        {
          return $this->_user_name;
        } else {
          return NULL;
        }
    }

    /* password
    ------------------------------------------*/

    /**
    * Set the _password
    *
    * @param string $password
    */

    public function setPassword($password)
    {
        $salt1  = "t%^l";
        $salt2  = "6$2!";
        $readyPass = $salt1 . $password . $salt2;

        $this->_password = hash('ripemd128', $readyPass);
    }

    /**
    * Get the _password
    *
    * @return string
    */

    public function getPassword()
    {
        if ( ! empty($this->_password) )
        {
          return $this->_password;
        } else {
          return NULL;
        }
    }

    /* first name
    ------------------------------------------*/

    /**
    * Set the _first_name
    *
    * @param string $firstName
    */

    public function setFirstName($firstName)
    {
        $this->_first_name = $firstName;
    }

    /**
    * Get the _first_name
    *
    * @return string
    */

    public function getFirstName()
    {
        if ( ! empty($this->_first_name) )
        {
          return $this->_first_name;
        } else {
          return NULL;
        }
    }

    /* middle name
    ------------------------------------------*/

    /**
    * Set the _middle_name
    *
    * @param string $middleName
    */

    public function setMiddleName($middleName)
    {
        $this->_middle_name = $middleName;
    }

    /**
    * Get the $middleName
    *
    * @return string
    */

    public function getMiddleName()
    {
        if ( ! empty($this->_middle_name) )
        {
          return $this->_middle_name;
        } else {
          return NULL;
        }
    }

    /* Last name
    ------------------------------------------*/

    /**
    * Set the _last_name
    *
    * @param string $lastName
    */

    public function setLastName($lastName)
    {
        $this->_last_name = $lastName;
    }

    /**
    * Get the $lastName
    *
    * @return string
    */

    public function getLastName()
    {
        if ( ! empty($this->_last_name) )
        {
          return $this->_last_name;
        } else {
          return NULL;
        }
    }

    /* Gender
    ------------------------------------------*/

    /**
    * Set the _gender
    *
    * @param string $gender
    */

    public function setGender($gender)
    {
        $this->_gender = $gender;
    }

    /**
    * Get the $gender
    *
    * @return string
    */

    public function getGender()
    {
        if ( ! empty($this->_gender) )
        {
          return $this->_gender;
        } else {
          return NULL;
        }
    }

    /* Email
    ------------------------------------------*/

    /**
    * Set the _gender
    *
    * @param string $email
    */

    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
    * Get the $email
    *
    * @return string
    */

    public function getEmail()
    {
        if ( ! empty($this->_email) )
        {
          return $this->_email;
        } else {
          return NULL;
        }
    }

    /* Registration date
    ------------------------------------------*/

    /**
    * Set the _registration_date
    *
    * @param string
    */

    public function setRegistrationDate($registrationDate)
    {
        $this->_registration_date = $registrationDate;
    }

    /**
    * Get the _registration_date
    *
    * @return string registration date
    */

    public function getRegistrationDate()
    {
        if ( isset($this->_registration_date) )
        {
          return $this->_registration_date;
        }
    }

}
