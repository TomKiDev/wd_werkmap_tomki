<?php
/* Connect to Database
------------------------------------------*/
function ConnectToDb()
{
  // Make connection
  $mysqlConnect = mysqli_connect( DB_HOST, DB_USER, DB_CRED, DB_NAME );

  // Check connection
  if(mysqli_connect_errno())
  {
    die("
      <h1>Er is iets fout gegaan</h1>
      <p>Verbinding met de database was mislukt</p>
      <p>Error {$mysqli->connect_errno}:{$mysqli->connect_error}</p>
      <p>Ga terug en probeer later nogmaals</p>
      ");
  }
  // Return the connection object
  return $mysqlConnect;
}
