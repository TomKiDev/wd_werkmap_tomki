<?php

/* Show all errors
------------------------------------------*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

/* Variables
------------------------------------------*/
$tafel = 5;
setType($tafel , "int"); // Set variable type
//echo getType($tafel);

/* Print to screen
------------------------------------------*/

/* First method using numbers
------------------------------------------*/
// Print the header for the page with the number defined under the variable
echo "<h1>Het table van $tafel:</h1>";
// Print the sum of multiplying 1 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">1 x $tafel = " . ( 1 * $tafel ) . "</p>";
// Print the sum of multiplying 2 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">2 x $tafel = " . ( 2 * $tafel ) . "</p>";
// Print the sum of multiplying 3 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">3 x $tafel = " . ( 3 * $tafel ) . "</p>";
// Print the sum of multiplying 4 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">4 x $tafel = " . ( 4 * $tafel ) . "</p>";
// Print the sum of multiplying 5 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">5 x $tafel = " . ( 5 * $tafel ) . "</p>";
// Print the sum of multiplying 6 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">6 x $tafel = " . ( 6 * $tafel ) . "</p>";
// Print the sum of multiplying 7 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">7 x $tafel = " . ( 7 * $tafel ) . "</p>";
// Print the sum of multiplying 8 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">8 x $tafel = " . ( 8 * $tafel ) . "</p>";
// Print the sum of multiplying 9 by the variable with a small left padding
echo "<p style=\"padding-left: 0.5rem;\">9 x $tafel = " . ( 9 * $tafel ) . "</p>";
// Print the sum of multiplying 10 by the variable
echo "<p>10 x $tafel = " . ( 10 * $tafel ) . "</p>";

die();
/* Second method using a loop and if statement
------------------------------------------*/
// Print the header for the page with the number defined under the variable
echo "<h1>Het table van $tafel:</h1>";

for ($i = 1; 11 > $i ; $i++) { // As long as the number is lower than 11 execute the following command
  if (10 > $i) { // As long as the number is lower than 10 print a paragraph with a small padding and the sum of multiplying the number by the variable
    echo "<p style=\"padding-left: 0.5rem;\">$i x $tafel = " . ( $i * $tafel ) . "</p>";
  } else { // If the number is 10 print the sum of multiplying 10 by the variable
    echo "<p>$i x $tafel = " . ( $i * $tafel ) . "</p>";
  }
}
