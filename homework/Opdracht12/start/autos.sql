﻿CREATE DATABASE `opdracht12`;

CREATE TABLE `autos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `merk_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `kleur` varchar(100) DEFAULT NULL,
  `brandstof` varchar(100) DEFAULT NULL,
  `zitplaatsen` int(2) DEFAULT NULL,
  `prijs` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `merk_id` (`merk_id`),
  CONSTRAINT `autos_ibfk_1` FOREIGN KEY (`merk_id`) REFERENCES `merken` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `autos` (`id`, `merk_id`, `title`, `type`, `kleur`, `brandstof`, `zitplaatsen`, `prijs`)
VALUES
	(1,6,'Eerste versie','Model 3','zwart','electra',5,590000),
	(2,2,'Lekker offroad','XC70','grijs','diesel',5,49000),
	(3,2,'Gezin','V70','wit','Hybrid',5,59000),
	(4,3,'Luxe','5','blauw','Hybrid',5,79000),
	(5,5,'First Sight','C8 Preliator','blauw','Hybrid',2,129000),
	(6,3,'Instap model','1','roze','Benzine',4,15000),
	(7,1,'Het begin','600','Geel','benzine',4,5000);

CREATE TABLE `merken` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) DEFAULT NULL,
  `land` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `merken` (`id`, `naam`, `land`)
VALUES
	(1,'Daf','Nederland'),
	(2,'Volvo','Zweden'),
	(3,'BMW','Duitsland'),
	(4,'Burton','Nederland'),
	(5,'Spyker','Nederland'),
	(6,'Tesla','Amerika');



/*
 * zorg ervoor dat ik alle errors zichtbnaar heb
 */
ini_set('error.reporting', E_ALL);
ini_set('display_errors', true);

/*
 * Br variabele voor een volgende regel in html en in de source
 */
$br = "<br>\n";

/*
 * verbinding maken met de mysql server en database
 * Bewaar een referentie naar de verbinding in een variabele
 */
$mysql_connection = mysqli_connect('localhost', 'root', 'root', 'cmm_wd313_autos');

/*
 * controleer of de verbinding gelukt is, laat andes een error zien
 */
if (mysqli_connect_errno()) {
    echo 'Connect failed:  ' . mysqli_connect_error();
    die();
}


/*
 * Maak een query om merken op te halen uit de tabel merken
 */
$query = 'SELECT id, naam, land FROM merken ORDER BY naam';
echo 'Query: ' . $query . $br . '<br>' . $br;

/*
 * Voer de query uit om een result object aan te maken
 * Omdat ik mysqli gebruik moet ik er voor zorgen dat ik elke keer het mysql-verbinding-object mee geef
 */
$mysqlResult = mysqli_query($mysql_connection, $query);

/*
 * hoeveel items heb ik terug gekregen?
 */
echo 'Aantal merken: ' . mysqli_num_rows($mysqlResult) . $br . '<br>' . $br;

/*
 * Maak een HTML tabel aan voor een betere weergave
 */
echo "<table border='1' cellspacing='0' cellpadding='2'>
	<tr>
		<th>Id</th>
		<th>Naam</th>
		<th>Land</th>
	</tr>";

/*
 * ik loop nu door het result object heen
 * en vertaal elke mysql regel naar een door php bruikbare array
 * Dit doe ik zolang ik een resultaat heb,
 * elke keer haal ik namelijk 1 regel uit het resultaat object
 */
while ($merk = mysqli_fetch_assoc($mysqlResult)) {
    /*
     * geef de gegevens van elke merk weer
     * Ik heb aangegeven met mysqli_fetch_assoc dat het een assosiative array is
     */
    echo '<tr>';
    echo '<td>' . $merk['id'] . '</td>';
    echo '<td>' . ucfirst($merk['naam']) . '</td>';
    echo '<td>' . $merk['land'] . '</td>';
    echo "</tr>\n";
}


echo '</table>';
