/* Create database
------------------------------------------*/
﻿CREATE DATABASE `opdracht12`;

/* Create table brands and insert info
------------------------------------------*/
CREATE TABLE `merken` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) DEFAULT NULL,
  `land` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `merken` (`id`, `naam`, `land`)
VALUES
	(1,'Daf','Nederland'),
	(2,'Volvo','Zweden'),
	(3,'BMW','Duitsland'),
	(4,'Burton','Nederland'),
	(5,'Spyker','Nederland'),
	(6,'Tesla','Amerika');

  /* Create table cars and insert info
  ------------------------------------------*/
CREATE TABLE `autos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `merk_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `kleur` varchar(100) DEFAULT NULL,
  `brandstof` varchar(100) DEFAULT NULL,
  `zitplaatsen` int(2) DEFAULT NULL,
  `prijs` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `merk_id` (`merk_id`),
  CONSTRAINT `autos_ibfk_1` FOREIGN KEY (`merk_id`) REFERENCES `merken` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `autos` (`id`, `merk_id`, `title`, `type`, `kleur`, `brandstof`, `zitplaatsen`, `prijs`)
VALUES
	(1,6,'Eerste versie','Model 3','zwart','electra',5,590000),
	(2,2,'Lekker offroad','XC70','grijs','diesel',5,49000),
	(3,2,'Gezin','V70','wit','Hybrid',5,59000),
	(4,3,'Luxe','5','blauw','Hybrid',5,79000),
	(5,5,'First Sight','C8 Preliator','blauw','Hybrid',2,129000),
	(6,3,'Instap model','1','roze','Benzine',4,15000),
	(7,1,'Het begin','600','Geel','benzine',4,5000);

/* 1. Create table countries and insert info
------------------------------------------*/
CREATE TABLE `landen`
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`land` varchar(255) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `landen` (`id`, `land`)
VALUES
	(NULL , `Nederland`),
  (NULL , `Zweden`),
  (NULL , `Duitsland`),
  (NULL , `Amerika`);

/* 2. link tables brands and countries
------------------------------------------*/

ALTER TABLE `merken` ADD `land_id` int(11) unsigned;
ALTER TABLE `merken` ADD FOREIGN KEY (`land_id`) REFERENCES `landen` (`id`);

UPDATE `merken` SET `land_id` = 1 WHERE `land` = 'Nederland';
UPDATE `merken` SET `land_id` = 2 WHERE `land` = 'Zweden';
UPDATE `merken` SET `land_id` = 3 WHERE `land` = 'Duitsland';
UPDATE `merken` SET `land_id` = 4 WHERE `land` = 'Amerika';

ALTER TABLE `merken` DROP `land`;

/* 3. Show all brands cars and countries
------------------------------------------*/
