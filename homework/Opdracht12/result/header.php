<!DOCTYPE html>
<html lang="nl">
<!-- Head
===============================-->
<head>
  <meta charset="utf-8">
  <title>Opdracht 12</title>
  <!-- Styles -->
  <style>
  body {
    width: 90%;
    margin: 0 auto;
  }
    label {
      display: block;
    }
    span {
      display: inline-block;
      width: 7em;
    }
    input {
      margin: 0.5em;
    }
    input[type="submit"] {
      display: inline;
      width: 5em;
      font-size: 1.2em;
      background-color: #0055FF;
    }
    tr {
      text-align: center;;
    }
    th {
      width:20em;
    }
  </style>
</head>
