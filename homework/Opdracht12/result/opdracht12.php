<?php
/* Show all errors
------------------------------------------*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

/* Insert header
------------------------------------------*/
require_once('header.php');
/* Connection
------------------------------------------*/
$connect = mysqli_connect('localhost', 'root', 'root', 'opdracht12');

if (mysqli_connect_errno()) {
    echo 'Connect failed:  ' . mysqli_connect_error();
    die();
}

/* Sanitize
------------------------------------------*/
function sanitize($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

/* Variables
------------------------------------------*/
$br = "<br>\n";
$merk = $land = $type = $kleur = $brandstof =  $and = "";
$soort = 'naam';
$volgorde = 'ASC';
/* min max price
------------------------------------------*/
$higestPriceQuery = "SELECT prijs FROM `autos`
                     ORDER BY prijs DESC
                     LIMIT 1;";
$higestPriceResult = mysqli_query($connect, $higestPriceQuery);
$higestPriceAssoc = mysqli_fetch_assoc($higestPriceResult);
$minprijs = 0;
$maxprijs = $higestPriceAssoc['prijs'];
/* min max sitting palces
------------------------------------------*/
$maxSitQuery = "SELECT zitplaatsen FROM `autos`
                ORDER BY zitplaatsen DESC
                LIMIT 1;";
$maxSitResult = mysqli_query($connect, $maxSitQuery);
$maxSitAssoc = mysqli_fetch_assoc($maxSitResult);
$minzitplaatsen = 0;
$maxzitplaatsen = $maxSitAssoc['zitplaatsen'];

/* Start page
------------------------------------------*/
echo "<h1>Opdracht 12</h1>";

/* 3. Show all brands cars and countries
------------------------------------------*/
/* Query for table info
------------------------------------------*/
$query = "SELECT naam, land, type, kleur, brandstof, zitplaatsen , prijs
          FROM `merken` AS m, `landen` AS l , `autos` AS a
          WHERE a.merk_id = m.id AND m.land_id = l.id
          ORDER BY $soort $volgorde;";
/* Reset query
------------------------------------------*/
if(isset($_POST['reset']))
{
  $query = "SELECT naam, land, type, kleur, brandstof, zitplaatsen , prijs
            FROM `merken` AS m, `landen` AS l , `autos` AS a
            WHERE a.merk_id = m.id AND m.land_id = l.id
            ORDER BY $soort $volgorde;";
}

/* Query for user input
------------------------------------------*/
if(isset($_POST['submit']))
{
  if (isset($_POST['merk']) && (!empty($_POST['merk'])))
  {
    $merk = ucfirst(sanitize( $_POST['merk'] ));
    $and .= " AND m.naam LIKE '%" . $merk .  "%' ";
  }
  if (isset($_POST['land']) && (!empty($_POST['land'])))
  {
    $land = ucfirst(sanitize($_POST['land']));
    $and .= " AND l.land LIKE '%" . $land .  "%' ";
  }
  if (isset($_POST['type']) && (!empty($_POST['type'])))
  {
    $type = ucfirst(sanitize($_POST['type']));
    $and .= " AND a.type LIKE '%" . $type .  "%' ";
  }
  if (isset($_POST['kleur']) && (!empty($_POST['kleur'])))
  {
    $kleur = ucfirst(sanitize($_POST['kleur']));
    $and .= " AND a.kleur LIKE '%" . $kleur .  "%' ";
  }
  if (isset($_POST['brandstof']) && (!empty($_POST['brandstof'])))
  {
    $brandstof = ucfirst(sanitize($_POST['brandstof']));
    $and .= " AND a.brandstof LIKE '%" . $brandstof .  "%' ";
  }
  if (isset($_POST['minzitplaatsen']) || (isset($_POST['maxzitplaatsen'])))
  {
    if (!empty($_POST['minzitplaatsen'])) {
      $minzitplaatsen = sanitize($_POST['minzitplaatsen']);
    }
    if (!empty($_POST['maxzitplaatsen'])) {
      $maxzitplaatsen = sanitize($_POST['maxzitplaatsen']);
    }
    $and .= " AND a.zitplaatsen BETWEEN $minzitplaatsen AND $maxzitplaatsen";
  }
  if (isset($_POST['minprijs']) || (isset($_POST['maxprijs'])))
  {
    if (!empty($_POST['minprijs'])) {
      $minprijs = sanitize($_POST['minprijs']);
    }
    if (!empty($_POST['maxprijs'])) {
      $maxprijs = sanitize($_POST['maxprijs']);
    }
    $and .= " AND a.prijs BETWEEN $minprijs AND $maxprijs";
  }
  if (isset($_POST['soort']))
  {
    $soort = sanitize($_POST['soort']);
  }
  if (isset($_POST['volgorde']))
  {
    $volgorde = sanitize($_POST['volgorde']);
  }
  $query = "SELECT naam, land, type, kleur, brandstof, zitplaatsen , prijs
            FROM `merken` AS m, `landen` AS l , `autos` AS a
            WHERE a.merk_id = m.id AND m.land_id = l.id" . $and . "
            ORDER BY $soort $volgorde;";
}


/* Query result
------------------------------------------*/
$result = mysqli_query($connect, $query);
$resultRows = mysqli_num_rows($result);

  echo '<body>';
  echo '<h2>Filter the result:</h2>';
  echo '<form method="post" action="opdracht12.php">';
  echo "<label><span>Merk:</span><input type=\"text\" name=\"merk\" value=\"$merk\"></label>";
  echo "<label><span>Land:</span><input type=\"text\" name=\"land\" value=\"$land\"></label>";
  echo "<label><span>Type:</span><input type=\"text\" name=\"type\" value=\"$type\"></label>";
  echo "<label><span>Kleur:</span><input type=\"text\" name=\"kleur\" value=\"$kleur\"></label>";
  echo "<label><span>Brandstof:</span><input type=\"text\" name=\"brandstof\" value=\"$brandstof\"></label>";
  echo "<label><span>Zitplaatsen:</span><input type=\"text\" name=\"minzitplaatsen\" value=\"$minzitplaatsen\">-<input type=\"text\" name=\"maxzitplaatsen\" value=\"$maxzitplaatsen\"></label>";
  echo "<label><span>Prijs Between:</span><input type=\"text\" name=\"minprijs\" value=\"$minprijs\">-<input type=\"text\" name=\"maxprijs\" value=\"$maxprijs\"></label>";
  echo '<input type="submit" name="submit" value="Filter">';
  echo '<input type="submit" name="reset" value="Reset">';
  echo 'Soorteer op: <select name="soort">
          <option value="naam">Merk</option>
          <option value="land">Land</option>
          <option value="type">Type</option>
          <option value="kleur">Kleur</option>
          <option value="brandstof">Brandstof</option>
          <option value="zitplaatsen">Zitplaatsen</option>
          <option value="prijs">Prijs</option>
        </select>';
  echo '<select name="volgorde">
          <option value="ASC">A-Z</option>
          <option value="DESC">Z-A</option>
        </select>';
  echo  '</form>';
if ( $resultRows == 0 ) {
  echo "<h3>Helaas, niks gevonden</h3>";
} else {
  echo '<table border=\'1\' cellspacing=\'0\' cellpadding=\'2\'>
  	<tr>
  		<th>Merk</th>
  		<th>Land</th>
      <th>Type</th>
      <th>Kleur</th>
      <th>Brandstof</th>
      <th>Zitplaatsen</th>
      <th>Prijs</th>
  	</tr>';

    while ($merk = mysqli_fetch_assoc($result)) {
        echo '<tr>';
        echo '<td>' . ucfirst($merk['naam']) . '</td>';
        echo '<td>' . $merk['land'] . '</td>';
        echo '<td>' . $merk['type'] . '</td>';
        echo '<td>' . $merk['kleur'] . '</td>';
        echo '<td>' . $merk['brandstof'] . '</td>';
        echo '<td>' . $merk['zitplaatsen'] . '</td>';
        echo '<td>' . $merk['prijs'] . '</td>';
        echo "</tr>\n";
    }
  echo '</table>';
}
echo '</body>';
