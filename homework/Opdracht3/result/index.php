<?php

/* Show all errors
------------------------------------------*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

/* Variable
------------------------------------------*/
$myVar = 'John';
setType($myVar , "string"); // Set variable type

/* Print to screen
------------------------------------------*/
# Content of variable:
echo "<p>De inhoud van mijn variabele is: <strong>$myVar</strong>.</p>";
# Type of variable:
echo "<p>Het type van mijn variabele is: <span style=\"font-weight: bold;\">" . getType($myVar) . '</span>.</p>';
