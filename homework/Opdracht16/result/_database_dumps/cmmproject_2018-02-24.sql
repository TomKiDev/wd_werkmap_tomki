create database cmmproject;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `password` char(128) NOT NULL DEFAULT '',
  `remember_token` varchar(150) NOT NULL DEFAULT '',
	`created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);


CREATE TABLE `news` (
  `id` int(11) NOT NULL,
	`user_id` int(11) NOT NULL,
	`title` varchar(255) NOT NULL DEFAULT '',
	`body` text NOT NULL,
  `publish_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
	`title` varchar(255) NOT NULL DEFAULT '',
	`url` varchar(255) NOT NULL DEFAULT '',
	`body` text NOT NULL,
	`active` enum('1', '0') NOT NULL DEFAULT '1',
	`user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
	ADD KEY `user_id` (`user_id`);


INSERT INTO `news` (`id`, `user_id`, `title`, `body`, `publish_date`, `created_at`, `updated_at`)
VALUES
	(1,0,'Titel','Duis sit amet dignissim ligula. Maecenas posuere vulputate gravida. Duis rhoncus sed arcu vel suscipit. Nam bibendum ante arcu, id cursus justo lacinia ut. Duis vestibulum massa sit amet tortor lobortis aliquam. Morbi in velit eget tellus rutrum semper. Etiam sodales semper neque, at porta sem. Fusce sit amet rutrum diam, ac ultricies sem.','2018-02-04','2018-02-04 13:00:25','2018-02-04 13:00:25'),
	(2,0,'title','Morbi justo dui, venenatis et luctus eget, ultricies vitae neque. Nulla tincidunt ultricies neque vitae consequat. Maecenas ultrices finibus diam at ornare. Mauris sed justo mauris. Aliquam placerat leo vitae purus sollicitudin tempor. Sed vehicula, urna at dignissim mollis, orci nibh lacinia elit, eget ornare massa sapien eu elit. Vivamus sapien ex, fringilla molestie ante nec, maximus pellentesque leo. Cras placerat bibendum nibh, eget condimentum quam pretium at. Nullam enim justo, scelerisque nec quam in, euismod dignissim ligula.\r\n\r\nMauris fermentum ullamcorper augue, sed bibendum nibh. Morbi nec imperdiet felis. Suspendisse cursus elit urna, non volutpat erat euismod quis. Etiam vel cursus mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris vehicula viverra lectus, nec sagittis leo rhoncus dignissim. Pellentesque ullamcorper velit a arcu fermentum elementum. Mauris fermentum augue justo, eget facilisis lectus dictum at. Nam elementum rhoncus enim at sodales. Vestibulum sed lorem felis. Nam non pellentesque elit. Phasellus vel lorem fringilla, accumsan ante eget, mollis nunc. Donec dapibus vitae justo non scelerisque.\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi scelerisque finibus nisl, vel ultrices diam sodales ut. Morbi id dolor sit amet turpis maximus cursus nec eget magna. Integer eu felis luctus, lobortis magna viverra, iaculis mi. Aenean tincidunt arcu id dolor ullamcorper, id egestas odio hendrerit. Nullam ornare enim diam, in tincidunt nibh posuere vitae. Vivamus sodales lacus at felis varius, vel pellentesque est efficitur. Mauris viverra ornare ante at porta. Donec suscipit metus felis, nec interdum purus ultrices ut. Vivamus id tellus dictum, blandit magna sed, scelerisque mauris. Sed eget semper odio. Aliquam ornare dapibus ligula ut pharetra.','2018-02-05','2018-02-05 13:49:41','2018-02-17 15:49:59'),
	(4,0,'Duis sit amet dignissim ligula','Duis sit amet dignissim ligula. Maecenas posuere vulputate gravida. Duis rhoncus sed arcu vel suscipit. Nam bibendum ante arcu, id cursus justo lacinia ut. Duis vestibulum massa sit amet tortor lobortis aliquam. Morbi in velit eget tellus rutrum semper. Etiam sodales semper neque, at porta sem. Fusce sit amet rutrum diam, ac ultricies sem.\n\nMorbi justo dui, venenatis et luctus eget, ultricies vitae neque. Nulla tincidunt ultricies neque vitae consequat. Maecenas ultrices finibus diam at ornare. Mauris sed justo mauris. Aliquam placerat leo vitae purus sollicitudin tempor. Sed vehicula, urna at dignissim mollis, orci nibh lacinia elit, eget ornare massa sapien eu elit. Vivamus sapien ex, fringilla molestie ante nec, maximus pellentesque leo. Cras placerat bibendum nibh, eget condimentum quam pretium at. Nullam enim justo, scelerisque nec quam in, euismod dignissim ligula.\n\nMauris fermentum ullamcorper augue, sed bibendum nibh. Morbi nec imperdiet felis. Suspendisse cursus elit urna, non volutpat erat euismod quis. Etiam vel cursus mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris vehicula viverra lectus, nec sagittis leo rhoncus dignissim. Pellentesque ullamcorper velit a arcu fermentum elementum. Mauris fermentum augue justo, eget facilisis lectus dictum at. Nam elementum rhoncus enim at sodales. Vestibulum sed lorem felis. Nam non pellentesque elit. Phasellus vel lorem fringilla, accumsan ante eget, mollis nunc. Donec dapibus vitae justo non scelerisque.\n\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi scelerisque finibus nisl, vel ultrices diam sodales ut. Morbi id dolor sit amet turpis maximus cursus nec eget magna. Integer eu felis luctus, lobortis magna viverra, iaculis mi. Aenean tincidunt arcu id dolor ullamcorper, id egestas odio hendrerit. Nullam ornare enim diam, in tincidunt nibh posuere vitae. Vivamus sodales lacus at felis varius, vel pellentesque est efficitur. Mauris viverra ornare ante at porta. Donec suscipit metus felis, nec interdum purus ultrices ut. Vivamus id tellus dictum, blandit magna sed, scelerisque mauris. Sed eget semper odio. Aliquam ornare dapibus ligula ut pharetra.','2018-01-15','2018-02-05 13:49:57','2018-02-05 13:49:57'),
	(8,0,'Morbi scelerisque finibus nisl','Morbi justo dui, venenatis et luctus eget, ultricies vitae neque. Nulla tincidunt ultricies neque vitae consequat. Maecenas ultrices finibus diam at ornare. Mauris sed justo mauris. Aliquam placerat leo vitae purus sollicitudin tempor. Sed vehicula, urna at dignissim mollis, orci nibh lacinia elit, eget ornare massa sapien eu elit. Vivamus sapien ex, fringilla molestie ante nec, maximus pellentesque leo. Cras placerat bibendum nibh, eget condimentum quam pretium at. Nullam enim justo, scelerisque nec quam in, euismod dignissim ligula.\r\n\r\nMauris fermentum ullamcorper augue, sed bibendum nibh. Morbi nec imperdiet felis. Suspendisse cursus elit urna, non volutpat erat euismod quis. Etiam vel cursus mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris vehicula viverra lectus, nec sagittis leo rhoncus dignissim. Pellentesque ullamcorper velit a arcu fermentum elementum. Mauris fermentum augue justo, eget facilisis lectus dictum at. Nam elementum rhoncus enim at sodales. Vestibulum sed lorem felis. Nam non pellentesque elit. Phasellus vel lorem fringilla, accumsan ante eget, mollis nunc. Donec dapibus vitae justo non scelerisque.\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi scelerisque finibus nisl, vel ultrices diam sodales ut. Morbi id dolor sit amet turpis maximus cursus nec eget magna. Integer eu felis luctus, lobortis magna viverra, iaculis mi. Aenean tincidunt arcu id dolor ullamcorper, id egestas odio hendrerit. Nullam ornare enim diam, in tincidunt nibh posuere vitae. Vivamus sodales lacus at felis varius, vel pellentesque est efficitur. Mauris viverra ornare ante at porta. Donec suscipit metus felis, nec interdum purus ultrices ut. Vivamus id tellus dictum, blandit magna sed, scelerisque mauris. Sed eget semper odio. Aliquam ornare dapibus ligula ut pharetra.','2018-02-05','2018-02-17 15:29:49','2018-02-17 15:29:49'),
	(9,0,'Morbi scelerisque finibus nisl','Morbi justo dui, venenatis et luctus eget, ultricies vitae neque. Nulla tincidunt ultricies neque vitae consequat. Maecenas ultrices finibus diam at ornare. Mauris sed justo mauris. Aliquam placerat leo vitae purus sollicitudin tempor. Sed vehicula, urna at dignissim mollis, orci nibh lacinia elit, eget ornare massa sapien eu elit. Vivamus sapien ex, fringilla molestie ante nec, maximus pellentesque leo. Cras placerat bibendum nibh, eget condimentum quam pretium at. Nullam enim justo, scelerisque nec quam in, euismod dignissim ligula.\r\n\r\nMauris fermentum ullamcorper augue, sed bibendum nibh. Morbi nec imperdiet felis. Suspendisse cursus elit urna, non volutpat erat euismod quis. Etiam vel cursus mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris vehicula viverra lectus, nec sagittis leo rhoncus dignissim. Pellentesque ullamcorper velit a arcu fermentum elementum. Mauris fermentum augue justo, eget facilisis lectus dictum at. Nam elementum rhoncus enim at sodales. Vestibulum sed lorem felis. Nam non pellentesque elit. Phasellus vel lorem fringilla, accumsan ante eget, mollis nunc. Donec dapibus vitae justo non scelerisque.\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi scelerisque finibus nisl, vel ultrices diam sodales ut. Morbi id dolor sit amet turpis maximus cursus nec eget magna. Integer eu felis luctus, lobortis magna viverra, iaculis mi. Aenean tincidunt arcu id dolor ullamcorper, id egestas odio hendrerit. Nullam ornare enim diam, in tincidunt nibh posuere vitae. Vivamus sodales lacus at felis varius, vel pellentesque est efficitur. Mauris viverra ornare ante at porta. Donec suscipit metus felis, nec interdum purus ultrices ut. Vivamus id tellus dictum, blandit magna sed, scelerisque mauris. Sed eget semper odio. Aliquam ornare dapibus ligula ut pharetra.','2018-02-05','2018-02-17 15:47:47','2018-02-17 15:47:47'),
	(10,0,'Het is gelukt','Das niet zo moeilijk toch?','2018-02-17','2018-02-17 15:58:17','2018-02-17 15:58:17'),
	(11,0,'Dan gaat we het nog een keer doen','Wat mag het wezen???','2018-02-17','2018-02-17 16:01:32','2018-02-17 16:01:32');



INSERT INTO `pages` (`id`, `title`, `url`, `body`, `active`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'Over ons','over_ons','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus placerat tortor a dignissim bibendum. Proin facilisis a est quis suscipit. Ut bibendum vehicula facilisis. Aliquam mauris odio, feugiat et magna eu, convallis tincidunt enim. Vestibulum eu euismod lectus, nec pharetra nisi.','1','1','2018-02-07 12:13:39','2018-02-07 12:13:39'),
	(2,'Contact','contact','Dit is de contact pagina. Als je contact met ons wilt opnemen dan moet je even mailen naar info@cmm.nl','0','1','2018-02-07 12:16:49','2018-02-07 12:16:54'),
	(3,'Welkom','welcome','Phasellus placerat tortor a dignissim bibendum. Proin facilisis a est quis suscipit. Ut bibendum vehicula facilisis. Aliquam mauris odio, feugiat et magna eu, convallis tincidunt enim. Vestibulum eu euismod lectus, nec pharetra nisi.','1','1','2018-02-07 12:56:09','2018-02-07 12:56:13');


INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Tomki','tom@ki.nl','$2y$10$zK8sQ96OUaSIUj4/hHbkpeydF5E7qnp4Ytc3FSfYdNJD/zvzQte.K','MW0n5UXid09HYFRLblz3eo386H5uxR2hFPZst8sijiHJICdjWiiXFunI9Sbw','2018-02-24 10:33:06','2018-02-24 10:33:06');
