var number        = 3;

// var changeNumber  = "";

function refresh(sel) {
    number = (sel.value);
    showNews(newsArray, number);
};


function showNews(newsArray, number = 3) {
  var newsMsg     = "";
  var newsArray   = newsArray[0]
  // console.log( "newsmsg is running ");

  newsMsg   +=  "<div id=\"newsContainer\" class=\"row mb-2\">";

  for ( i =0 ; i < number ; i++) {

    body    =   newsArray[i]["body"];
    body    =   body.substr(0, 50);
    link    =   "http://cmm16.localhost:8000/news/";

    newsMsg +=  "<div class=\"col-md-6\">";
    newsMsg +=  "<div class=\"card flex-md-row mb-4 box-shadow h-md-120\">";
    newsMsg +=  "<div class=\"card-body d-flex flex-column align-items-start\">";
    newsMsg +=  "<h3 class=\"mb-0\">";
    newsMsg +=  "<a href=\"" + link + newsArray[i]["slug"] + "\">" + newsArray[i]["title"] + "</a></h3>";
    newsMsg +=  "<div class=\"mb-1 text-muted\">" + newsArray[i]["publish_date"] + "</div>";
    newsMsg +=  "<p class=\"card-text mb-auto\">" + body + "</p>";
    newsMsg +=  "<small class=\"text-muted\"><a href=\"" + link + newsArray[i]["slug"] + "\">Lees meer</a></small>";
    newsMsg +=  "</div></div></div>";
    // console.log( "mid newsmsg is: " + newsMsg );
  }
  newsMsg   +=  "</div>";
  // console.log( "newsmsg is: " + newsMsg );
  $( "#newsContainer" ).replaceWith(newsMsg);
};

$.ajax({

  url       : "http://cmm16.localhost:8000/ajax/news",
  method    : "GET",
  dataType  : "json",
  success   : function( data ) {
    newsAmount  = data.length;
    newsArray   = new Array( data );
    newsArray.sort(function(obj1, obj2){
      return obj1[publish_date] - obj2[publish_date];
    });

    html    =   "<form id=\"callNews\" action=\"\" method=\"get\">";
    html    +=  "<label for=\"amount\">Zie de laatste ";
    html    +=  "<select name=\"numberOfNews\" id=\"numberOfNews\" onchange=\"refresh(this)\">";
    html    +=  "<option value=\"3\">3</option>";
    html    +=  "<option value=\"6\">6</option>";
    html    +=  "<option value=\"" + newsAmount + "\">All</option>";
    html    +=  "</select>";
    html    +=  " nieuwsberichten</label>";
    html    +=  "</form>";
    html    +=  "<div id=\"newsContainer\">News Berichten komen hier terecht</div>";

    $( "#ajaxNews" ).replaceWith(html);
    showNews(newsArray, 3 );
  }
});
