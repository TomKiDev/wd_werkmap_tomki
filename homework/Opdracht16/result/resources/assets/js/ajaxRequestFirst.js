/**
 * @return JSON file converted to news list
 */

function callAjaxNews(){
  nocache     = "&nocache=" + Math.random() * 1000000;
  request     = new ajaxRequest();

  request.open( "GET", "/ajax/news" + nocache , true );

  request.onreadystatechange = function()
  {
    if( this.readystate == 4 && this.status == 200 )
    {
      if (this.responseText != null)
      {
        result  = this.responseText;
        allNews = JSON.parse(result);
        newsAmount = allNews.length;

        html    =   "<form method=\"get\">";
        html    +=  "<label for=\"amount\">Zie de laatste";
        html    +=  "<select>";
        html    +=  "<option value=\"3\">3</option>";
        html    +=  "<option value=\"6\">6</option>";
        html    +=  "<option value=\"" + newsAmount + "\">All</option>";
        html    +=  "</select>";
        html    +=  "nieuwsberichten</label>";
        html    +=  "</form>";

        document.getElementById("ajaxNews").innerHTML = html;
      }
      else alert( "Ajax error: No data received" )
    }
    else alert ( "Ajax error: " + this.statusText )
  }

  request.send();

  function ajaxRequest()
  {
    try // Non IE Browser
    { // Yes
      var request = new XMLHttpRequest()
    }
    catch(e1)
    {
      try // IE 6+?
      { // Yes
        request = new ActiveXObject("Msxml2.XMLHTTP")
      }
      catch(e2)
      {
        try // IE 5?
        { // Yes
          request = new ActiveXObject("Microsoft.XMLHTTP")
        }
        catch(e3) // There is no Ajax Support
        {
          request = false
        }
      }
    }
    return request;
  }
}

callAjaxNews();
