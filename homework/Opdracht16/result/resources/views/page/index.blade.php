@extends( 'layout.master' )

@section('content')

    <section class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="jumbotron-heading">{{ $page->title }}</h1>
            <p>
                {{ $page->body }}
            </p>
            <p>
                <a href="/welcome/" class="btn btn-primary" role="button">Welkom</a>
                <a href="/contact/" class="btn btn-primary" role="button">Contact</a>
                <a href="/over_ons/" class="btn btn-primary" role="button">Over ons</a>
                <a href="/news/" class="btn btn-primary" role="button">Nieuws</a>
            </p>
        </div>
    </section>

    <div class="mb-2">
        <div id="ajaxNews">
          <!-- preloader animatie -->
          <img id="preload" src="../images/preload.gif" alt="Loading" />
        </div>
    </div>
@endsection
