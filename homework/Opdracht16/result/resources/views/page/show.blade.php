@extends( 'layout.master' )

@section('title')
    {{ $page->title }}
@endsection

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
            <h1>{{ $page->title }}</h1>
            <p>
                {{ $page->body }}
            </p>
        </div>
    </div>
@endsection
