<?php
/* Page settings
---------------------------*/
require_once( '../../settings/config.php' );
/* Set page and call header
---------------------------*/
$page = 'news';
require_once( MAIN . '/head.php' );
?>
    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <?php
          $h1 = 'CMM Nieuws';
          $h1 = strtolower($h1);
          $h1 = ucfirst($h1);
        ?>
        <h1><?php echo $h1; ?></h1>
        <p>Lees meer over de laatste ontwikkelingen bij CMM</p>
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <?php
      /* News array
      =======================================*/
      $news =   [
        'opendag' => [
          'newsTitle'   => 'Volg een proefles tijdens de OPEN DAG op 19 november ',
          'newsBericht' => 'Ben je op zoek naar een multimedia opleiding met toekomst? Kom dan op zondag 19 november naar de open dag van College of MultiMedia. Volg een proefles in Webdesign, 3D',
        ],
        '3D' => [
          'newsTitle'   => 'Wat ik bedenk, kan ik maken in 3D. Hoe gek het ook is.',
          'newsBericht' => 'Maikel Baarda werkt al ruim 16 jaar in de tv-wereld. Ervaring deed hij op bij Eyeworks, RTL en SBS6 en hij heeft ook bij Media Landscape gewerkt waar hij commercials',
        ],
        'tekort' => [
          'newsTitle'   => 'Tekort aan Developers neemt toe',
          'newsBericht' => 'De digitalisering van de maatschappij en van de economie voltrekt zich in een hoog tempo. Omdat voor steeds meer ondernemingen ICT een bepalende factor speelt, is de vraag naar professionals',
        ],
        'facebook' => [
          'newsTitle'   => 'Nieuwe training: Masterclass Facebook Adverteren!',
          'newsBericht' => 'Binnen een marketingstrategie kan en mag adverteren op Facebook voor veel bedrijven en instellingen eigenlijk niet meer ontbreken. Facebook is een krachtig medium om de doelgroep te bereiken, leads te',
        ],
        'adobe' => [
          'newsTitle'   => 'Nieuw in Adobe Creative Cloud 2018',
          'newsBericht' => 'Tijdens het grootste Adobe evenement van het jaar, Adobe Max in Las Vegas, op 18 oktober jongstleden heeft Adobe een aantal mooie nieuwe apps en functies gepresenteerd. Als Adobe Authorised',
        ],
        'opMaat' => [
          'newsTitle'   => 'Training op maat',
          'newsBericht' => 'College of MultiMedia biedt ook trainingen en opleidingen op maat. De locatie, het aantal deelnemers en de inhoud worden jouw wensen afgestemd. Bezoek onze maatwerk pagina voor meer informatie over de mogelijkheden. OF neem direct contact op voor meer informatie en een vrijblijvende offerte.',
        ],
        'komLangs' => [
          'newsTitle'   => 'Kom bij ons langs!',
          'newsBericht' => 'Kun je niet langskomen op een open dag of gratis proefles en wil je hulp bij het bepalen van de juiste studiekeuze? Maak dan gerust een afspraak met ons voor een rondleiding en een persoonlijk adviesgesprek met één van onze docenten. Je kunt ons bellen op 020 – 462 39 39 of stuur een email.',
        ],
        'opleiding' => [
          'newsTitle'   => 'Opleidingen',
          'newsBericht' => 'Bekijk onze opleiding pagina. Klik op de opleiding voor full-time of part-time leren!',
        ],
      ];

      /* Create bericht for each news item
      =======================================*/
      foreach ($news as $key => $bericht) {

        $h2 = $bericht['newsTitle'];
        $p = $bericht['newsBericht'];

        echo '<div class="col-lg-4">';
        echo "<h2>$h2</h2>";
        echo '<p>' . date("l jS \of F Y h:i:s A") . '</p>';
        echo "<p>$p</p>";
        echo '<p><a class="btn btn-default" href="includes/bericht.php">View details &raquo;</a></p>';
        echo '</div>';
      };
      ?>
    </div>
    <hr>
    <!-- Footer
    ===============================-->
    <?php
      require_once( MAIN . '/footer.php' );
    ?>
