<!-- Navigation bar
===============================-->
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Tomki</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <!-- Define active page in class
        ===============================-->
        <li class="<?if($page=="home"){?>active<?}?>"><a href="<?=PUBLIC_ROOT?>/index.php">Home</a></li>
        <li class="<?if($page=="aboutme"){?>active<?}?>"><a href="<?=PAGES?>/aboutme.php">About Me</a></li>
        <li class="<?if($page=="news"){?>active<?}?>"><a href="<?=NEWS?>/news.php">News</a></li>
        <li class="<?if($page=="contact"){?>active<?}?>"><a href="#contact">Contact</a></li>
      </ul>
      <!-- Login form
      ===============================-->
      <?php include_once('login.php'); ?>
    </div>
    <!--/.navbar-collapse -->
  </div>
</div>
