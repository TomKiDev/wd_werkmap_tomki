<?php
  if( isset( $_POST['email'] ) && isset( $_POST['password'] ) ) {
    $_SESSION['ingelogd'] = 'Ja';
    $_SESSION['email']    = sanitize($_POST['email']);
  }

  if ( isset( $_POST['signOut'] ) && $_POST['signOut'] == 'signOut' ) {
    $_SESSION['ingelogd'] = 'Nee';
    unset($_SESSION['ingelogd']);
    unset($_SESSION['img']);
    $_SESSION['img']    = IMG . '/cmm-logo@.png';
    $loginName = '';
  }


  if ( isset( $_SESSION['ingelogd'] ) && ('Ja' == $_SESSION['ingelogd']) ) {
    $loginName = $_SESSION['email'];
    echo '<form class="navbar-form navbar-right" method="post">';
    echo '<button type="submit" name="signOut" value="signOut" class="btn btn-success">User '. $loginName . ' Sign out</button>';
    echo '</form>';
  } else {
    echo '<form class="navbar-form navbar-right" method="post">';
    echo '<div class="form-group">';
    echo '<input type="text" name="email" placeholder="Email" class="form-control">';
    echo '</div>';
    echo '<div class="form-group">';
    echo '<input type="password" name="password" placeholder="Password" class="form-control">';
    echo '</div>';
    echo '<button type="submit" name="signIn" class="btn btn-success">Sign in</button>';
    echo '</form>';
  }
