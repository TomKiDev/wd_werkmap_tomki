<?php
/* Start session if not existing
------------------------------------------*/
if ( ! session_id() ) {
    session_start();
}

/* Show all errors
------------------------------------------*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

/* Define root and constants
------------------------------------------*/

// If at develop stage at home
if (strpos($_SERVER['DOCUMENT_ROOT'], 'F:/WDV-17/working') !== FALSE)
{
  define( 'DOCUMENT_ROOT' , 'F:/WDV-17/working/homework13Jan/Opdracht10' );
	define( 'DB_HOST' , 'localhost');
	define( 'DB_USER' , 'root');
	define( 'DB_CRED' , 'root');
	define( 'DB_NAME' , '');
}
// If ready to check for Git
elseif (strpos($_SERVER['DOCUMENT_ROOT'], 'F:/WDV-17/WD_werkmap_tomki/homework') !== FALSE)
{
  define( 'DOCUMENT_ROOT' , 'F:/WDV-17/WD_werkmap_tomki/homework/Opdracht10' );
	define( 'DB_HOST' , 'localhost');
	define( 'DB_USER' , 'root');
	define( 'DB_CRED' , 'root');
	define( 'DB_NAME' , '');
}
// If at school
elseif (strpos($_SERVER['DOCUMENT_ROOT'], '/Volumes/CMMTomerK/WDV-17/working') !== FALSE)
{
  define( 'DOCUMENT_ROOT' , '/Volumes/CMMTomerK/WDV-17/working/homework13Jan/Opdracht10' );
	define( 'DB_HOST' , 'localhost');
	define( 'DB_USER' , 'root');
	define( 'DB_CRED' , 'root');
	define( 'DB_NAME' , '');
}
// fallback
else
{
  define( 'DOCUMENT_ROOT' , $_SERVER['DOCUMENT_ROOT'] );
	define( 'DB_HOST' , 'localhost');
	define( 'DB_USER' , 'root');
	define( 'DB_CRED' , 'root');
	define( 'DB_NAME' , '');
}

define( 'WEB_ROOT' , '//' . $_SERVER['HTTP_HOST'] ); // Web root
define( 'SITE_ROOT' , DOCUMENT_ROOT . '/public' ); // Site root
define( 'LIBRARY' , DOCUMENT_ROOT . '/library' ); // Library
define( 'MAIN' , LIBRARY . '/views/main' ); // Main
define( 'PAGES' , WEB_ROOT . '/homework13Jan/Opdracht10/library/views/pages' ); // Pages
define( 'NEWS' , WEB_ROOT . '/homework13Jan/Opdracht10/library/views/news' ); // News
define( 'PUBLIC_ROOT' , WEB_ROOT . '/homework13Jan/Opdracht10/public' ); // Public
define( 'IMG' , PUBLIC_ROOT . '/images' ); // Images
define( 'IMG_ROOT' , SITE_ROOT . '/images' ); // Images
define( 'CSS' , PUBLIC_ROOT . '/css' ); // CSS
define( 'JS' , PUBLIC_ROOT . '/js' ); // JS
define( 'BR' , "<br>\n" ); // line break
