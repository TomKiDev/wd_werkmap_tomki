<?php
/* Create title
=======================================================================--*/
function createTitle( $page ) {
	$title = 'College of MultiMedia | Developer | ' . $page; // Add page name to title
	$title = strtolower( $title ); // Edit title to lower case
	$title = ucFirst( $title ); // Edit first letter to upper case
	$title = str_replace( ' ', '', $title ); // Remove spaces

	# Return the edited title
	return $title;
}
