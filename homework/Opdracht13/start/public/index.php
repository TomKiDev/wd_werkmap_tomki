<?php
/* Page settings
---------------------------*/
require_once( '../library/settings/config.php' );
/* Set page and call header
---------------------------*/
$page = 'home';
require_once( MAIN . '/head.php' );
?>
    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <?php
          $h1 = 'My first php project';
          $h1 = strtolower($h1);
          $h1 = ucfirst($h1);
        ?>

        <h1><?php echo $h1; ?></h1>
        <p>Welcome to my home page</p>
        <p><a href="<?=PAGES?>/aboutme.php" class="btn btn-primary btn-lg">Read more about me&raquo;</a></p>
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <div class="col-lg-4">
        <h2>Bericht 1</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
            condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
            euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
      </div>
      <div class="col-lg-4">
        <h2>Bericht 2</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
            condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
            euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
      </div>
      <div class="col-lg-4">
        <h2>Bericht 3</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
            condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
            euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
      </div>
      <div class="col-lg-4">
        <h2>Bericht 4</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
            condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
            euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
      </div>
    </div>
    <hr>
    <!-- Footer
    ===============================-->
    <?php
      require_once( MAIN . '/footer.php' );
    ?>
