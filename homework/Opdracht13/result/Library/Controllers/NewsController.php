<?php
/* Require News Model
---------------------------*/
require_once( DOCUMENT_ROOT . '/Library/Models/News.php');

/* News Controller functions
---------------------------*/
class NewsController
{
  /* variables
  ------------------------------------------*/
  protected $_mysqli;

  private function ConnectToDb()
  {
    if( empty($this->_mysqli) ) {
      // Make connection
      $this->_mysqli = new mysqli( DB_HOST, DB_USER, DB_CRED, DB_NAME );

      // Check connection
      if($this->_mysqli->connect_errno)
      {
        die("
          <h1>Er is iets fout gegaan</h1>
          <p>Verbinding met de database was mislukt</p>
          <p>Error " . $this->_mysqli->connect_errno . " : " . $this->_mysqli->connect_error . "</p>
          <p>Ga terug en probeer later nogmaals</p>
          ");
      }
      // Return the connection object
      return $this->_mysqli;
    }
  }

  /**
  * Get all the news messages
  *
  * @return array of messages
  */
  public function getAllNews()
  {
    // Make connection
    $mysqli = $this->connectToDb();
    // query for all news messages
    $query = " SELECT p.*, c.title AS category, concat( u.username, ' ', u.middlename, ' ', u.lastname ) AS user
            FROM `posts` AS p
            INNER JOIN `categories` AS c ON c.id=p.category_id
            LEFT JOIN `users` AS u ON u.id=p.user_id
            WHERE p.`publish_date` IS NOT NULL
            AND p.`publish_date` <= NOW()
            ORDER BY `publish_date`";

    // Make messages array
    $allNews = array();
    // if any result found
    if( $result = $mysqli->query($query) ) {
      // loop threw the result to make the array of messages
      while ( $news = $result->fetch_assoc() )
      {
        // Make News message model
        $newsObject = new NewsModel( $news );
        // Add to the messages array
        $allNews[] = $newsObject;
      }
      // Return all the messages
      return $allNews;
    }
  }





  /**
  * Show all the news messages
  *
  * @return list of HTML
  */
  public function showNewsList()
  {
    $newsList = $this->getAllNews();
    require_once( NEWS . 'NewsListPage.php' );
  }


//if ( empty($newsUrl) ) return showNewsList();

  /**
  * Show one news message according to id or url
  *
  * @return newsModel of HTML
  */
  public function showNewsMessage( $newsUrl )
  {
    // Make connection
    $mysqli = $this->connectToDb();
    // query for news message based on url
    if ( ! $query = $mysqli->prepare("SELECT p.*, c.title AS category, concat( u.username, ' ', u.middlename, ' ', u.lastname ) AS user
            FROM `posts` AS p
            INNER JOIN `categories` AS c ON c.id=p.category_id
            LEFT JOIN `users` AS u ON u.id=p.user_id
            WHERE p.`publish_date` IS NOT NULL
            AND p.`publish_date` <= NOW()
            AND (p.post_url=? OR p.id=?)")
			) {
			throw new Exception("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // bind the get url
    $query->bind_param("si", $newsUrl['news'], $newsUrl['id'] );


    // excute query
    if ( ! $query->execute()) {
			throw new Exception("Execute failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // Get the result
    $result = $query->get_result();
    $item = $result->fetch_assoc();

    // Assign result to news model
    $newsPage = new newsModel( $item);

    // Check no news message found set title to nothing found
    if ( empty( $newsPage->getTitle() ) ) {
      $newsPage->setTitle('Helaas geen bericht kunnen vinden');
    }
    // Return news model
    return $newsPage;
  }

  public function EditSingleNews()
  {
    // Make connection
    $mysqli = $this->connectToDb();
    // query to match user id
    if ( ! $query = $mysqli->prepare("SELECT id
              FROM users
              WHERE id = ?)")
			) {
			throw new Exception("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // bind the get url
    $query->bind_param("i", $_SESSION['user_id'] );


    // excute query
    if ( ! $query->execute()) {
			throw new Exception("Execute failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

    // Get the result
    $result = $query->get_result();
    $item = $result->fetch_assoc();

    // Assign result to news model
    $newsPage = new newsModel( $item);

    // Check no news message found set title to nothing found
    if ( empty( $newsPage->getTitle() ) ) {
      $newsPage->setTitle('Helaas geen bericht kunnen vinden');
    }
    // Return news model
    return $newsPage;
  }
}
