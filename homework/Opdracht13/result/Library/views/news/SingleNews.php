    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <h1><?php echo $newsPage->getTitle(); ?></h1>
        <p><small><?php echo $newsPage->getCategoryTitle(); ?></small><p>
        <p><?php echo $newsPage->getUserName(); ?><p>
        <p>Bericht Datum: <?php echo $newsPage->getPublishDate(); ?><p>
        <p>Aangepast Om: <?php echo $newsPage->getLastUpdated(); ?><p>
        <?php
          if (! empty( $loginName ) ) {
            echo '<p><a href="' . WEB_NEWS . '/news.php?news=' . $newsPage->getId() . '/edit" class="btn btn-primary btn-lg">Edit this news message</a></p>';
          }
        ?>
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <p><b><?php echo $newsPage->getIntro(); ?></b><p>
      <p><?php echo $newsPage->getMessage(); ?><p>
    </div>
