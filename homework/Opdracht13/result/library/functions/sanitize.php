<?php
function sanitize($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function beautify($value) {
  $value = strtolower($value);
  $value = ucfirst($value);
}
