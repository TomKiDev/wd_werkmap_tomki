<?php

/* News model class
------------------------------------------*/

class NewsModel
{
  /* variables
  ------------------------------------------*/
  protected $_id;
	protected $_category_id;
	protected $_post_url;
	protected $_user_id;
	protected $_title;
	protected $_intro;
	protected $_message;
	protected $_image_source;
	protected $_publish_date;
	protected $_creation_date;
	protected $_last_updated;
	protected $_category_title;
	protected $_user_name;

  /* Construct
  ------------------------------------------*/
  public function __construct ( $defaultVars=array() )
	{
		// Loop threw the array
		foreach( $defaultVars as $key => $value )
		{
			// turn key name to name
			$key = str_replace(" ", "", ucwords( str_replace("_", " ", $key) ) );
			$setter = "set". ucfirst($key);

			// check if setter exists inside the class
			if ( method_exists($this, $setter) ) {
				$this->$setter( $value );
			}
		}
	}

  /* Get and Set
  ------------------------------------------*/

  /* id
  ------------------------------------------*/

  /**
  * Set the _id
  *
  * @param Int $id
  */

  public function setId($id)
  {
    if ( is_numeric($id) ) {
      $this->_id = $id;
    }
  }

  /**
  * Get the id
  *
  * @return Int
  */

  public function getId()
  {
      if ( ! empty($this->_id) )
      {
        return $this->_id;
      } else {
        return NULL;
      }
  }

  /* category id
  ------------------------------------------*/

  /**
  * Set the _category_id
  *
  * @param Int $category_id
  */

  public function setCategoryId($category_id)
  {
    if ( is_numeric($category_id) ) {
      $this->_category_id = $category_id;
    }
  }

  /**
  * Get the category_id
  *
  * @return Int
  */

  public function getCategoryId()
  {
      if ( ! empty($this->_category_id) )
      {
        return $this->_category_id;
      } else {
        return NULL;
      }
  }

  /* user id
  ------------------------------------------*/

  /**
  * Set the _user_id
  *
  * @param Int $user_id
  */

  public function setUserId($user_id)
  {
      $this->_user_id = $user_id;
  }

  /**
  * Get the user_id
  *
  * @return Int
  */

  public function getUserId()
  {
      if ( isset($this->_user_id) )
      {
        return $this->_user_id;
      }
  }

  /* title
  ------------------------------------------*/

  /**
  * Set the _title
  *
  * @param string $title
  */

  public function setTitle($title)
  {
      $this->_title = $title;
  }

  /**
  * Get the _title
  *
  * @return string title
  */

  public function getTitle()
  {
      if ( isset($this->_title) )
      {
        return $this->_title;
      }
  }

  /* post url
  ------------------------------------------*/

  /**
  * Set the _post_url
  *
  * @param string
  */

  public function setPostUrl($title)
  {
      $url = str_replace(array(' ', '.'), array('_', ''), $title);
      $this->_post_url = strToLower( urlencode( $url) );
  }

  /**
  * Get the _post_url
  *
  * @return string post url
  */

  public function getPostUrl()
  {
      if ( empty($this->_post_url) )
      {
        $this->setPostUrl( $this->getTitle() );
      }
      if ( isset($this->_post_url) )
      {
        return $this->_post_url;
      }
  }

  /* intro
  ------------------------------------------*/

  /**
  * Set the _intro
  *
  * @param text
  */

  public function setIntro($intro)
  {
      $this->_intro = $intro;
  }

  /**
  * Get the _intro
  *
  * @return text intro
  */

  public function getIntro()
  {
      if ( isset($this->_intro) )
      {
        return $this->_intro;
      }
  }

  /* message
  ------------------------------------------*/

  /**
  * Set the _message
  *
  * @param text
  */

  public function setMessage($message)
  {
      $this->_message = $message;
  }

  /**
  * Get the _message
  *
  * @return text message
  */

  public function getMessage()
  {
      if ( isset($this->_message) )
      {
        return $this->_message;
      }
  }

  /* image source
  ------------------------------------------*/

  /**
  * Set the _image_source
  *
  * @param string
  */

  public function setImageSource($ImageSource)
  {
      $this->_image_source = $ImageSource;
  }

  /**
  * Get the _image_source
  *
  * @return string image source
  */

  public function getImageSource()
  {
      if ( isset($this->_image_source) )
      {
        return $this->_image_source;
      }
  }

  /* publish date
  ------------------------------------------*/

  /**
  * Set the _publish_date
  *
  * @param string
  */

  public function setPublishDate($publishDate)
  {
      $this->_publish_date = $publishDate;
  }

  /**
  * Get the _publish_date
  *
  * @return string publish date
  */

  public function getPublishDate()
  {
      if ( isset($this->_publish_date) )
      {
        return $this->_publish_date;
      }
  }

  /* creation date
  ------------------------------------------*/

  /**
  * Set the _creation_date
  *
  * @param string
  */

  public function setCreationDate($creationDate)
  {
      $this->_creation_date = $creationDate;
  }

  /**
  * Get the _creation_date
  *
  * @return string creation date
  */

  public function getCreationDate()
  {
      if ( isset($this->_creation_date) )
      {
        return $this->_creation_date;
      }
  }

  /* last updated
  ------------------------------------------*/

  /**
  * Set the _last_updated
  *
  * @param string
  */

  public function setLastUpdated($lastUpdated)
  {
      $this->_last_updated = $lastUpdated;
  }

  /**
  * Get the _last_updated
  *
  * @return string last updated
  */

  public function getLastUpdated()
  {
      if ( isset($this->_last_updated) )
      {
        return $this->_last_updated;
      }
  }

  /* category title
  ------------------------------------------*/

  /**
  * Set the _category_title
  *
  * @param string
  */

  public function setCategoryTitle($categoryTitle)
  {
      $this->_category_title = $categoryTitle;
  }

  /**
  * Get the _category_title
  *
  * @return string category title
  */

  public function getCategoryTitle()
  {
      if ( isset($this->_category_title) )
      {
        return $this->_category_title;
      }
  }

  /* user name
  ------------------------------------------*/

  /**
  * Set the _user_name
  *
  * @param string
  */

  public function setUserName($userName)
  {
      $this->_user_name = $userName;
  }

  /**
  * Get the _user_name
  *
  * @return string user name
  */

  public function getUserName()
  {
      if ( isset($this->_user_name) )
      {
        return $this->_user_name;
      }
  }

}
