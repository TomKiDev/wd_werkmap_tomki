<?php

$pagesArray = [
                    'pageTitle'   =>  [
                                    'newsPage' => 'College of MultiMedia | News'
                                      ],
                    'pageHeader'  =>  [
                                    'newsPage' => 'CMM Nieuws'
                                      ],
                    'news' =>       [
                      'newsTitle'   =>    [
                                    'opendag'   => 'Volg een proefles tijdens de OPEN DAG op 19 november ',
                                    '3D'        => 'Wat ik bedenk, kan ik maken in 3D. Hoe gek het ook is.',
                                    'tekort'    => 'Tekort aan Developers neemt toe',
                                    'facebook'  => 'Nieuwe training: Masterclass Facebook Adverteren!',
                                    'adobe'     => 'Nieuw in Adobe Creative Cloud 2018',
                                    'opMaat'    => 'Training op maat',
                                    'komLangs'  => 'Kom bij ons langs!',
                                    'opleiding' => 'Opleidingen'
                                          ],
                      'newsBericht' =>    [
                                    'opendag'   => 'Ben je op zoek naar een multimedia opleiding met toekomst? Kom dan op zondag 19 november naar de open dag van College of MultiMedia. Volg een proefles in Webdesign, 3D',
                                    '3D'        => 'Maikel Baarda werkt al ruim 16 jaar in de tv-wereld. Ervaring deed hij op bij Eyeworks, RTL en SBS6 en hij heeft ook bij Media Landscape gewerkt waar hij commercials',
                                    'tekort'    => 'De digitalisering van de maatschappij en van de economie voltrekt zich in een hoog tempo. Omdat voor steeds meer ondernemingen ICT een bepalende factor speelt, is de vraag naar professionals',
                                    'facebook'  => 'Binnen een marketingstrategie kan en mag adverteren op Facebook voor veel bedrijven en instellingen eigenlijk niet meer ontbreken. Facebook is een krachtig medium om de doelgroep te bereiken, leads te',
                                    'adobe'     => 'Tijdens het grootste Adobe evenement van het jaar, Adobe Max in Las Vegas, op 18 oktober jongstleden heeft Adobe een aantal mooie nieuwe apps en functies gepresenteerd. Als Adobe Authorised',
                                    'opMaat'    => 'College of MultiMedia biedt ook trainingen en opleidingen op maat. De locatie, het aantal deelnemers en de inhoud worden jouw wensen afgestemd. Bezoek onze maatwerk pagina voor meer informatie over de mogelijkheden. OF neem direct contact op voor meer informatie en een vrijblijvende offerte.',
                                    'komLangs'  => 'Kun je niet langskomen op een open dag of gratis proefles en wil je hulp bij het bepalen van de juiste studiekeuze? Maak dan gerust een afspraak met ons voor een rondleiding en een persoonlijk adviesgesprek met één van onze docenten. Je kunt ons bellen op 020 – 462 39 39 of stuur een email.',
                                    'opleiding' => 'Bekijk onze opleiding pagina. Klik op de opleiding voor full-time of part-time leren!'
                                          ],
                                    ];
];
