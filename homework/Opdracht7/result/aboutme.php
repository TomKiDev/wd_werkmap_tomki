<?php
  /* Page settings
  ---------------------------*/
  require_once('includes/setting.php');
  /* Page functions
  ---------------------------*/
  require_once('includes/functions.php');
?>
<!DOCTYPE html>
<html lang="nl">
<!-- Head
===============================-->
<head>
  <?php
    $page = 'aboutme';
    /* Page header
    ---------------------------*/
    require_once('includes/head.php');
  ?>
</head>
<!-- Body
===============================-->
<body>
<!-- Header
===============================-->
  <header>
    <!-- Navigatie
    ===============================-->
    <?php
      require_once('includes/nav.php');
    ?>
    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <div class="col-lg-6" >
          <?php
            $h1 = 'About Me';
            $h1 = strtolower($h1);
            $h1 = ucfirst($h1);
          ?>
          <h1><?php echo $h1; ?></h1>
          <p>Dit is de about pagina van mijn project</p>
        </div>
        <img class="col-lg-6" src="images/cmm-logo@.png" alt="CMM College of Multimedia" />
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <div class="col-lg-4">
        <h2>Bericht 1</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
            condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
            euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
      </div>
      <div class="col-lg-4">
        <h2>Bericht 2</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
            condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
            euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
      </div>
      <div class="col-lg-4">
        <h2>Bericht 3</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
            condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
            euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
      </div>
      <div class="col-lg-4">
        <h2>Bericht 4</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
            condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
            euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
      </div>
    </div>
    <hr>
    <!-- Footer
    ===============================-->
    <?php
      require_once('includes/footer.php');
    ?>
    </div>
    <!-- Scriptts
    ===============================-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>
