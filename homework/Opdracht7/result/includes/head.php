<meta charset="utf-8">
<link rel="shortcut icon" href="favicon.ico">

<!-- Page title -->
<?php
if ( ! isset($page)) {
  $page = 'welkom';  
}
?>

<title><?php echo createTitle($page); ?></title>
<!-- Styles -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
