<!-- Navigation bar
===============================-->
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Tomki</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <!-- Define active page in class
        ===============================-->
        <li class="<?if($page=="home"){?>active<?}?>"><a href="index.php">Home</a></li>
        <li class="<?if($page=="aboutme"){?>active<?}?>"><a href="aboutme.php">About Me</a></li>
        <li class="<?if($page=="news"){?>active<?}?>"><a href="news.php">News</a></li>
        <li class="<?if($page=="contact"){?>active<?}?>"><a href="#contact">Contact</a></li>
      </ul>
      <!-- Login form
      ===============================-->
      <form class="navbar-form navbar-right">
        <div class="form-group">
          <input type="text" placeholder="Email" class="form-control">
        </div>
        <div class="form-group">
          <input type="password" placeholder="Password" class="form-control">
        </div>
        <button type="submit" class="btn btn-success">Sign in</button>
      </form>
    </div>
    <!--/.navbar-collapse -->
  </div>
</div>
