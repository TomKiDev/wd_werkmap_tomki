<?php
/* Upload Image
=======================================================================--*/
/* Define default image
------------------------------------------*/
if (! isset($_SESSION['img'])) {
  $_SESSION['img']    = IMG . '/cmm-logo@.png';
  $_SESSION['title']  = 'CMM College of Multimedia';
}
$imgMsg    = 'Would you like to replace the image?';
$msgErr    = 'Click on browse to search for your file';
$uploadOk  = 1;

/* Check if posted and upload new image
------------------------------------------*/
if ( isset( $_POST['uploadImg'] ) ) {
	/* Get and set file information
	------------------------------------------*/  
	# Basename is niet nodig in dit geval
	$imgFileName = $_FILES['newImage']['name'];
	$imgFile     = IMG_ROOT . '/' . $imgFileName;
	$imgFileType = strtolower( pathinfo( $imgFile, PATHINFO_EXTENSION ) );
	$imgSize     = $_FILES['newImage']['size'];

	/* Check file type
	------------------------------------------*/
	if ( ( $uploadOk == 1 ) && ( $imgFileType != 'jpg' && $imgFileType != 'png' && $imgFileType != 'gif' ) ) {
		$msgErr    = 'Sorry, only JPG PNG & GIF files are allowed.';
		$uploadOk  = 0; // Set upload to not ok
	}

	/* Check file size is below 2GB
	------------------------------------------*/
	if ( ( $uploadOk == 1 ) && ( $imgSize > 2000000 ) ) {
		$msgErr    = 'Sorry, file size must be under 2GB.';
		$uploadOk  = 0; // Set upload to not ok
	}

	/* After all checks are ok
	------------------------------------------*/
	if ( $uploadOk == 1 ) {
		//$img       = str_replace( '../', '', $imgFile );
		move_uploaded_file( $_FILES['newImage']['tmp_name'], IMG_ROOT . '/' . $imgFileName );
		$imgTitle = $imgFileName;
		$imgTitle = preg_replace( '/\\.[^.\\s]{3,4}$/', '', $imgTitle );
		$imgTitle = str_replace( 'images/', '', $imgTitle );
    $_SESSION['img']    = IMG . '/' . $imgFileName;
    $_SESSION['title']  = $imgTitle;
		$imgMsg   = $msgErr = 'Your image was changed to ' . $imgFileName;
	}
}
