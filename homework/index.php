<?php
ini_set('error_reporting', E_ALL);
setlocale(LC_TIME, "Dutch");

$dir = preg_replace('/[a-zA-Z]*\.php/', '', $_SERVER['SCRIPT_FILENAME']);
$path = preg_split('/\//', $dir, -1, PREG_SPLIT_NO_EMPTY);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="nl" xml:lang="nl">
<head>
    <title>Local - <?php echo $dir; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <style>
        #holder {
            margin  : 50px auto;
            width   : 600px;
            padding : 0 5px;
        }
        a {
            color           : #4F3E25;
        }
        td {
            padding          : 2px 6px;
            background-color : #fff;
        }
        tr:nth-child(even) td {
            background-color : #eee;
        }
    </style>
</head>

<body>
<div id="holder">
    <h1>Overzicht <?php echo ucfirst(end($path)); ?></h1>
    <table border="0" cellspacing="0" cellpadding="0">
        <?php

        $i      = 0;
        $dirArr = $subDir = [];
        $dh     = $d2 = '';

        // GET DIR IN ARR
        if (is_dir($dir) && $dh = opendir($dir)) {
            while (false !== ($file = readdir($dh))) {
                if ('.' == substr($file, 0, 1)) {
                    continue;
                }
                if (is_dir($file)) {
                    $d2     = opendir($dir . "/" . $file);
                    $subDir = [];
                    while (false !== ($file2 = readdir($d2))) {
                        if ('.' == substr($file2, 0, 1)) {
                            continue;
                        }
                        $subDir[] = $file2;
                    }
                    closedir($d2);
                    $dirArr[ $file ] = $subDir;
                } else {
                    $dirArr[] = $file;
                }
            }
            closedir($dh);
        }
        //asort($dirArr);

        // OUTPUT
        foreach ($dirArr as $folderName => $dir) {
            if ( ! is_array($dir)) {
                echo "<tr><td><a href=\"{$dir}\">{$dir}</a></td></tr>\n";
                continue;
            }

            echo "<tr><td><b>{$folderName}</b></td></tr>\n";
            foreach ($dir as $subDir) {
                echo "<tr><td>";
                echo "&nbsp;&nbsp;<a href=\"{$folderName}/{$subDir}\">{$folderName}&nbsp;:&nbsp;{$subDir}</a>";
                echo "</td></tr>\n";
            }
        }
        ?>
    </table>

</body>
</html>
