<?php
/* Variable
----------------------------*/
$tafel = 12; // Set table variable
$count = 0; // Set counter at zero

/* Print to screen
----------------------------*/
echo "<h1>Het table van $tafel:</h1>";

// Loop to create table lines
for ($num = 33 ; $count < 68 ; $num++) { // Between 33 and 100 - total 67 lines
  if (( $tafel * $num == 420) || ($num == 41)){ // If the total is 420 OR the number to multiply is 41
    continue; // Skip it
  } else if ( $tafel * $num > 790) { // If the total is higher then 790
    break; // Leave this loop
  }
  $count++; // Add 1 to the counter

  switch ($count % 2) { // calculate if line is even or uneven
    case '0': // If even
      $kleur = '#000'; // Set color to black
      break; // Go back to begin switch
    case '1': // If uneven
      $kleur = '#F00'; // Set color to black
      break; // Go back to begin switch
    default: // Default color is black
      $kleur = '#000';
  }
  // Print calculation and answer to screen
  echo "<p style= \" color : " . $kleur . ";\">" . $tafel . ' X ' . $num . ' = ' . $tafel * $num . '</p>';
};
// Print how many lines there are to screen
echo '<p>Er zijn <strong>' . $count . '</strong> regels</p>';
