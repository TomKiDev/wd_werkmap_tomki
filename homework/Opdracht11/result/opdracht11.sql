-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 07, 2018 at 11:06 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opdracht11`
--

-- --------------------------------------------------------

--
-- Table structure for table `fruit`
--

CREATE TABLE `fruit` (
  `id` int(10) NOT NULL,
  `name` varchar(25) NOT NULL,
  `in_box` int(5) NOT NULL,
  `price` float NOT NULL,
  `color` varchar(15) NOT NULL COMMENT 'De kleur in tekst',
  `weight` decimal(3,0) NOT NULL COMMENT 'Het gewicht van het stuk fruit'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fruit`
--

INSERT INTO `fruit` (`id`, `name`, `in_box`, `price`, `color`, `weight`) VALUES
(1, 'kersen', 50, 0.03, 'rood', 1),
(2, 'banaan', 15, 0.25, 'geel', 15),
(3, 'appel', 12, 0.15, 'geel', 25),
(4, 'radijs', 40, 0.02, 'rood', 2),
(5, 'meloen', 5, 1, 'groen', 100),
(6, 'komkommer ', 10, 0.1, 'groen', 10),
(7, 'sla', 6, 0.35, 'groen', 8),
(8, 'citroen', 3, 0.42, 'geel', 2);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(10) NOT NULL,
  `description` varchar(25) NOT NULL,
  `fruit_id` int(10) NOT NULL,
  `amount` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `description`, `fruit_id`, `amount`) VALUES
(1, 'Daroyal ', 1, 25),
(2, 'babybanaan', 2, 50),
(3, 'Christine ', 1, 2),
(4, 'Granny Smith', 3, 12),
(5, 'Florence', 1, 5),
(6, 'Ronde helderrode', 4, 12),
(7, 'Granny Smith', 3, 0),
(8, 'Bakbanaan', 2, 1),
(9, 'Giant Cavendish', 2, 6),
(10, 'Cherry Belle', 4, 12),
(11, 'Rode banaan', 2, 1),
(12, 'Gele Tros', 6, 23),
(13, 'Green Finger', 6, 34),
(14, 'Snack komkomer', 6, 7),
(15, 'Berg sla', 7, 19),
(16, 'kropsla', 7, 28),
(17, 'IJsbergsla', 7, 53),
(18, 'Citroen', 8, 78),
(19, 'Limoen', 8, 64);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fruit`
--
ALTER TABLE `fruit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `description` (`description`),
  ADD KEY `stock_fruit_relatie` (`fruit_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fruit`
--
ALTER TABLE `fruit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `stock_fruit_relatie` FOREIGN KEY (`fruit_id`) REFERENCES `fruit` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
