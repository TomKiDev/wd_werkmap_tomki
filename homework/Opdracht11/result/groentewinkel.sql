CREATE DATABASE opdracht11;

CREATE TABLE `fruit` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `in_box` int(5) NOT NULL,
  `price` float NOT NULL,
  `color` varchar(15) NOT NULL COMMENT 'De kleur in tekst',
  `weight` decimal(3,0) NOT NULL COMMENT 'Het gewicht van het stuk fruit',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `fruit` (`id`, `name`, `in_box`, `price`, `color`, `weight`)
VALUES
	(1,'aardbei',50,0.03,'rood',1),
	(2,'banaan',15,0.25,'geel',15),
	(3,'appel',12,0.15,'geel',25),
	(4,'radijs',40,0.02,'rood',2),
	(5,'meloen',5,1,'groen',100);

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) NOT NULL,
  `fruit_id` int(10) NOT NULL,
  `amount` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `description` (`description`),
  KEY `stock_fruit_relatie` (`fruit_id`),
  CONSTRAINT `stock_fruit_relatie` FOREIGN KEY (`fruit_id`) REFERENCES `fruit` (`id`)
) ENGINE=InnoDB;

INSERT INTO `stock` (`id`, `description`, `fruit_id`, `amount`)
VALUES
	(1,'Daroyal ',1,25),
	(2,'babybanaan',2,50),
	(3,'Christine ',1,2),
	(4,'Granny Smith',3,12),
	(5,'Florence',1,5),
	(6,'Ronde helderrode',4,12),
	(7,'Granny Smith',3,0),
	(8,'Bakbanaan',2,1),
	(9,'Giant Cavendish',2,6),
	(10,'Cherry Belle',4,12),
	(11,'Rode banaan',2,1);


/* 2. Maak een nieuwe row aan in de fruit tabel (3 gemaakt)
========================================================*/

INSERT INTO `fruit`
VALUES
  (6, 'komkommer ', 10, 0.1, 'groen', 10),
  (7, 'sla', 6, 0.35, 'groen', 8),
  (8, 'citroen', 3, 0.42, 'geel', 2);

/* 3. Link nieuwe rows in de stock tabel aan dit nieuwe fruit
========================================================*/
INSERT INTO `stock`
VALUES
  (NULL, 'Gele Tros', 6, 23),
  (NULL, 'Green Finger', 6, 34),
  (NULL, 'Snack komkomer', 6, 7),
  (NULL, 'Berg sla', 7, 19),
  (NULL, 'kropsla', 7, 28),
  (NULL, 'Ijsbergsla', 7, 53),
  (NULL, 'Citroen', 8, 78),
  (NULL, 'Limoen', 8, 64);

/* 4. Hoeveel verschillende soorten fruit zijn er?
========================================================*/
SELECT COUNT(*) AS 'totaal'
FROM `fruit`;

/* 5. Hoeveel fruit is er in totaal in voorraad?
========================================================*/
SELECT SUM(amount)
FROM `stock`;

/* 6. Hoeveel aardbeien zijn er in voorraad?
========================================================*/
SELECT SUM(amount)
FROM `stock`
WHERE fruit_id=1;


/* 7. Welk fruit is het duurst?
========================================================*/
SELECT * FROM `fruit` ORDER BY `price` DESC LIMIT 1;
/* kan ook: */
SELECT name, MAX(price) FROM `fruit` WHERE price = (SELECT MAX(price) FROM `fruit`);


/* 8. Welk item is het goedkoopst?
========================================================*/
SELECT * FROM `fruit` ORDER BY `price` ASC LIMIT 1;
/* kan ook: */
SELECT name, MIN(price) FROM `fruit` WHERE price = (SELECT MIN(price) FROM `fruit`);


/* 9. Laat per fruitsoort alle types zien
========================================================*/
SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id && fruit.id = 1
ORDER BY fruit.name ASC;

SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id && fruit.id = 2
ORDER BY fruit.name ASC;

SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id && fruit.id = 3
ORDER BY fruit.name ASC;

SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id && fruit.id = 4
ORDER BY fruit.name ASC;

SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id && fruit.id = 5
ORDER BY fruit.name ASC;

SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id && fruit.id = 6
ORDER BY fruit.name ASC;

SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id && fruit.id = 7
ORDER BY fruit.name ASC;

SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id && fruit.id = 8
ORDER BY fruit.name ASC;

/*10.  Laat alle fruitsoorten zien en alle fruit-types, zorg ervoor dat er geen dubbele zijn
========================================================*/

SELECT fruit.id, fruit.name AS 'Fruit name', stock.description AS 'Fruit soort'
FROM `fruit`, `stock`
WHERE fruit.id = stock.fruit_id
ORDER BY fruit.name ASC;

/* 11. Maak een query om de complete voorraad van de aardbeien te verwijderen
========================================================*/
DELETE FROM stock
JOIN ON fruit.id = stock.fruit_id
WHERE fruit.name = 'aardbei';

/* 12. Plaats drie nieuwe aardbeien regels in de voorraad
========================================================*/
INSERT INTO `stock`
VALUES
	(1,'Daroyal',1,25),
  (3,'Christine',1,2),
  (5,'Florence',1,5);

/* 13. Maak een query om alle aardbeien te veranderen in kersen
========================================================*/
UPDATE fruit
SET name = 'kersen'
WHERE name = 'aardbei';
