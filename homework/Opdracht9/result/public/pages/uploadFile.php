<?php
/* Page settings
---------------------------*/
require_once( '../../library/settings/config.php' );
/* Set page and call header
---------------------------*/
$page = 'aboutme';
require_once( MAIN . '/head.php' );
?>
    <!-- Jumbotron
    --------------------------------------------->
    <div class="jumbotron">
      <div class="container">
        <div class="col-lg-6" >
          <?php
            $h1 = 'About Me';
            $h1 = strtolower($h1);
            $h1 = ucfirst($h1);
          ?>
          <h1><?php echo $h1; ?></h1>
          <p>Please upload your new image using the form below</p>
        </div>
        <img class="col-lg-6" src="<?=IMG?>/aboutImage.png" alt="About me image" />
      </div>
    </div>
  </header>
  <!-- Content
  ===============================-->
    <div class="container">
      <!-- Form upload image
      ===============================-->
      <h2><?= $msgErr ?></h2>
      <form action="uploadFile.php" method="post" enctype="multipart/form-data">
        <input type="file" name="newImage" accept=".png, .jpg, .gif" required="required">
        <br>
        <input class="btn btn-primary btn-lg" type="submit" name="uploadImg" value="Upload Image">
      </form>
    </div>
    <hr>
    <!-- Footer
    ===============================-->
    <?php
      require_once( MAIN . '/footer.php' );
    ?>
